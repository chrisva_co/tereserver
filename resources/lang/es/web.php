<?php
//Esta se&ntilde;al estar&aacute; disponible...
return [
    'rep_dem' => "<center><h2>".
                  "Demasiadas personas viendo la opcion <br /><span class='glyphicon glyphicon-signal'></span><br /><br class='hidden-xxs' />".
                  "<h3>Para ver: <br /> Actualizar la opcion <button class='btn btn-warning' onclick='location.reload()'>Click aqu&iacute;</button> o cambia de <span class='glyphicon glyphicon-signal'></span> se&ntilde;al <span class='hidden-xxs'>abajo</span> </h3></center>",
    
    'rep_minutos_antes' => "<center><h2>".
                  "Resumen disponible en horas aproximadas al encuentro <br /><span class='glyphicon glyphicon-bell'></span><br /><br class='hidden-xxs' />".
                  "<h3>Para ver: <br /> Actualizar la opcion <button class='btn btn-warning' onclick='location.reload()'>Click aqu&iacute;</button> o cambia de <span class='glyphicon glyphicon-signal'></span> se&ntilde;al <span class='hidden-xxs'>abajo</span> </h3></center>",
    'mas' => 'Más',
    'ampliar' => 'Ampliar',    
    'regresar' => 'Regresar',    
    'senal' => 'Se&ntilde;al',    
    'compartir' => 'Compartir',
    'contacto' => 'Contacto',
    'slogan' => 'Canal de tv en vivo y en directo',
    'favoritos' => 'Favoritos',
    'web_desc' => 'Mira en vivo y en directo todos los encuentros deportivo del mundo en habla hispana',
    'about_title'=>'Sobre el Proyecto',
    'about'=>'Nuestro objetivo es crear un servicio web donde puedas ver todos tus canales favoritos del mundo sin necesidad de pagar por cualquier canal, ni descargar ni instalar nada. 

            <br /><br />
            Si encuentras imperfecciones, o tienes buenas ideas, síguenos 
            en <a target="_blank" class="btn btn-xs btn-info" href="https://www.facebook.com/'.env('FANPAGE_ID').'">facebook</a> y envíanos un mensaje.',

    'en_vivo' => 'En directo',
    'en_directo' => 'En vivo',
    'Edtvo' => 'Estaciones de television online',

    'reproducir' => 'Reproducir',
    'entrar_con_facebook' => 'Entrar con Facebook',

    'sobre_nosotros' => 'Sobre nosotros',
    'logout' => 'Salir',
    'ubicacion' => 'Ubicacion',
    'copyRight' => '© 2017. Tu television online de resumenes deportivos por internet',
    
    'comentarios' => 'Comentarios',
    'informacion' => 'Información',
    
    'otros_canales' => 'Encuentros de hoy',
    'buscar' => 'Buscar',
    
    'buscar' => 'Buscar',
    'visitas' => 'Visitas',
    'de' => 'de', //se cambia en ingles por ":"

    'cat' => 'Competiciones',
   'reg' => 'Regiones',
   'algunos_paises' => 'Algunos paises',

    'canales_' => '',
    'canales' => 'Canales',

    'canales_de' => 'Futbol TV ',
    'canal_' => 'en-vivo/',
    'canal' => 'En vivo',

    'cats' => ['Amistoso', 'Ascenso MX',
                            'Bundesliga',
                            'Calcio Serie B',
                            'Championship',
                            'Confederaciones',
                            'Copa de Francia',
                            'Copa America',
                            'Copa Del Rey',
                            'Eurocopa Francia',
                            'Eurocopa Leage',
                            'FA Cup',
                            'Jupiler Pro League',
                            'La Liga Promises',
                            'Liga Adelante',
                            'Liga BBVA',
                            'Liga MX',
                            'Liga de Campeones',
                            'Liga NOS',
                            'Mundial Femenino',
                            'Mundial Rusia 2018',
                            'Premier League',
                            'Supercopa Europa',
                            'Tercera Division',
                            'UEFA Youth League'],

     'paises' => ['Argentina', 'Bolivia',
                            'Chile',
                            'Colombia',
                            'Ecuador',
                            'España',
                            'Estados Unidos',
                            'Peru',
                            'Uruguay',
                            'Mexico',
                            'Venezuela'],

    //en este toca cambiar tambien el indice del array de traduccion
    'regions' => ['norte america'=>'Norte America',
                                //'centro america'=>'Centro America',
                                'sur america'=>'Sur America',
                                'europa'=>'Europa',
                                'africa'=>'Africa',
                                'asia'=>'Asia',
                                'oceania'=>'Oceania'],

    'todas_cats' => 'Todas las categorias',


];
