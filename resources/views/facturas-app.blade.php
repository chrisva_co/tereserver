@extends('layouts.base')

@section('title')
	Admin - Facturas
@stop

@section('js')
	<script src="{{asset('/js/maintools.js')}}"></script>
	<script src="{{asset('/js/iframe-auto-height.js')}}"></script>

	<script type="text/javascript">

		function mostrarDetalles(id){
			document.location = "{!!asset('factura/')!!}/"+id+"?m=1";
		}
	</script>
@stop

@section('body') 
	<style type="text/css">
		.fixedTop {
		    margin-top: 0px;
		}
	</style>
	
	<div class="container">
		<div class="row">
			<div class="col-sm-12">	

				<h3 class="hidden">Mis facturas</h3>		
<br/>
				<table class="table table-striped table-hover ">
				  <thead style="border:1px solid #CCC">
				    <tr>
				      <th>Fecha</th>
				      <th>Total</th>
				      <th>Estado</th>
				    </tr>
				  </thead>
				  <tbody>

				  

				    @foreach($facturas as $id=>$post)
				    	@if ($post->getCantidadPrecio()['valorTotal'])
							<tr onclick="mostrarDetalles('{{$post->id}}')">					
						     	<td>{{$post->created_at->format('j F Y')}}</td>
						      	<td>{{($post->getCantidadPrecio()['valorTotal'])." ".$config->moneda}}</td>
						      	<td>
						     	 	{!!($post->pagado)?'<span class="glyphicon glyphicon-ok text-success"> </span>':'<span class="glyphicon glyphicon-warning-sign text-warning"> </span>'!!}
						      	</td>
						    </tr>
						@endif
					@endforeach
				      
				   
				  </tbody>
				</table> 
				@if(count($facturas))

					<div style="text-align:center">
						{!! $facturas->render() !!}
					</div>

				@endif
				
			</div>
		</div>

	</div>
@stop