@extends('layouts.base-web')

@section('title')
Admin
@stop

@section('body') 
	<div class="container">
		<div class="row">
			<div class="col-sm-3">

	            

				@include('bloques/menu_principal')

			</div>


			<div class="col-sm-9">
				<h2>Lista de grupos
					<a data-fancybox-type="iframe" href="grupo" class="fancybox-iframe-no-modal btn btn-info">Añadir un grupo</a></h2>
				<hr/>

					@foreach($grupos as $id=>$grupo)
						<div class="">
							id:{{$grupo->id}} - {{$grupo->categoria}}: <b>{{$grupo->nombre}}</b> ({{$grupo->getCantidadFacturas()}}) <a href="{!!asset('grupo/'.$grupo->id)!!}" class="btn btn-info btn-xs fancybox-iframe-no-modal inline"  data-fancybox-type="iframe"  >Editar</a>
						</div>
					@endforeach
			</div>
		</div>


	</div>
@stop