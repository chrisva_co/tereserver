@extends('layouts.base-web')

@section('descripcion')
Descripcion
@stop


@section('js')

@stop


@section('title')
	Entrar a Tere
@stop

@section('body')
	<div class="container">
	
			<div class="row">
				<h1 class="center"> </h1><br>

				<div class="col-sm-12">

				@if(true)
					@include('bloques/entrar-con')
				@else
					<div class="web-center form-registro">
						<h2>Entrar al administrador</h2>
						<form method="post" class="frmUnico" id="entrar">
						
			        		<br class="hidden-xs" />
							<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

							<div class="input-group">
							  <span class="input-group-addon ">  <span class="glyphicon glyphicon-user"></span> </span>
							 <input class="form-control" required="required" placeholder="Nombre de usuario" maxlength="60" name="codigo" type="text">
							</div>
							
							<br />
							<div class="input-group ">
							  <span class="input-group-addon ">  <span class="glyphicon glyphicon-lock"></span> </span>
							  <input class="form-control" name="password" placeholder="Contraseña" maxlength="40" required="" type="password">
							</div>
							<br />
			        		<input class=" btn btn-primary right" name="btn-entrar" value="Entrar" type="submit">        
			        	</form>
			        </div>
			    @endif
					<br/>
			</div>

	</div>

</div>

   
@stop
