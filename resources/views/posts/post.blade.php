@extends('layouts.base-web')

@section('title') Titulo del post @stop

@section('imagen')imagen-del-post.jpg @stop

@section('descripcion')Descripcion del post @stop

@section('keywords') palabras, claves, del, post @stop

@section('js')

	<script>
		
	</script>

@stop

@section('body')

<div class="container">
	<h1 class="center x-center h1">Titulo del post</h1>
	<br>
	<div class="well well-sm hidden-xs">
		Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.

		<br>
		<div class="fb-share-button fb-share-button-c" data-href="{{asset(Lang::get('web.canal_'))}}" data-layout="button_count"></div>
								
			<a href="https://twitter.com/share" class="twitter-share-button" data-via="letradecancionx" data-lang="es"></a>
			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

			<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
			<g:plusone size="medium" width="300px"></g:plusone>
	</div>


	<div class="row">
		<div class="col-sm-3">
		publicidad
		</div>

		<div class="col-sm-9">
		video embed
		</div>
	</div>




	<br/>
	<div class="row">


	<div class="col-xs-12">

		<ul class="nav nav-tabs">
				  <li class=""><a href="#home" data-toggle="tab" aria-expanded="true"><span class="glyphicon glyphicon-comment"></span> {{Lang::get('web.comentarios')}}</a></li>
				  <li class="hidden-xs"><a href="#embed" data-toggle="tab" aria-expanded="false"><span class="glyphicon glyphicon-share"></span> Embed</a></li>
				  <li class="active"><a href="#profile" data-toggle="tab" aria-expanded="false"><span class="glyphicon glyphicon-info-sign"></span> {{Lang::get('web.informacion')}}</span></a></li>
		</ul>



		<div id="myTabContent" class="tab-content web-tab-content ">
				  	
				  	<div class="tab-pane fade web-comentarios" id="home">
					    <div class="fb-comments" data-href="{{asset(Lang::get('web.canal_'))}}" data-numposts="5" data-width="100%" ></div>
					</div>

				
				<div class="tab-pane fade" id="embed">
				  	<p>Copia y pega este codigo en tu sitio WEB o Blog</p>
					<textarea class="form-control input">&lt;iframe width=&quot;560&quot; height=&quot;315&quot; src=&quot;{{asset('embed-')}}&quot; allowfullscreen=&quot;true&quot; webkitallowfullscreen=&quot;true&quot; scrolling=&quot;no&quot; frameborder=&quot;0&quot;&gt; &lt;/iframe&gt;</textarea>	    
				</div>


				
				<div class="tab-pane fade active in" id="profile">
				   
				    <p><b>{{Lang::get('web.ubicacion')}}:</b> Pais</p>
				    <p><b>{{Lang::get('web.visitas')}}:</b> 12345</p>
				    <p><b>{{Lang::get('web.tags')}}:</b> tag1, tag2, tag3, tag4</p>
				  
				</div>
		</div>
	</div>



	</div>
</div>

@stop
