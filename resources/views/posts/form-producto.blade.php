@extends('layouts.base')

@section('body') 
<!--'user_id', 'barCode', 'nombre', 'descripcion', 'valorUnitario','categorias', 'formato','image','ubicacion','marca','activo','conIva','tipo','extra'-->

	<div class="container">
		<div class="row">
			
				<!--<h2>Añadir un evento</h2>-->
				<form method="post" class="frmUnico" id="entrar">
					<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

					<input type="hidden" name="idDocente" value="{!!Input::old('idDocente', isset($post)?$post->idDocente:1)!!}">

				<div class="row">
					<div class="col-sm-8">
						<b>Nombre</b>
						<br/>
						<input type='text' id="nombre" class="form-control" name="nombre"  value="{!!Input::old('nombre', isset($post)?$post->nombre:null)!!}" />
					</div>
					<div class="col-sm-4">
						<b>Precio (COP)</b>
						<br/>
						<input type='text' id="valorUnitario" class="form-control" name="valorUnitario"  value="{!!Input::old('valorUnitario', isset($post)?$post->valorUnitario:null)!!}" />
					</div>
				</div>
				<br>

				<div class="row">
					<div class="col-sm-5">
						<b>Marca</b>
						<br/>
						<input type='text' id="marca" class="form-control" name="marca"  value="{!!Input::old('marca', isset($post)?$post->marca:null)!!}" />
					</div>
					<div class="col-sm-3">
						<b>Con IVA</b>
						<br/>
						 {!! Form::select('conIva', 
						 [1=>'Si ('.$config->iva.'%)',0=>'No'], Input::old('conIva', isset($post)?$post->conIva:1),['class'=>'form-control']); !!}
						
					</div>
					<div class="col-sm-4">
						<b>Codigo de barras</b>
						<br/>
						<input type='text' id="barCode" class="form-control" name="barCode"  value="{!!Input::old('barCode', isset($post)?$post->barCode:null)!!}" />
					</div>
				</div>
				<br>

				<div class="row">
					<div class="col-sm-4">
						<b>Categoria</b>
						<br/>

						{!! Form::select('categorias', $config->categorias, Input::old('categorias', isset($post)?$post->categorias:null),['class'=>'form-control js-example-basic-single']) !!}

					</div>
					<div class="col-sm-4">
						<b>Formato</b>
						<br/>
						 {!! Form::select('formato', 
						 ['Peso (Kg)'=>'Peso (Kg)','Unidad'=>'Unidad'], Input::old('formato', isset($post)?$post->formato:null),['class'=>'form-control']); !!}
					</div>
					<div class="col-sm-4">
						<b>Ubicacion</b>
						<br/>
						<input type='text' id="ubicacion" class="form-control" name="ubicacion"  value="{!!Input::old('ubicacion', isset($post)?$post->ubicacion:null)!!}" placeholder="ej: Lácteos" />
					</div>
				</div>
				<br />

				<div class="row">
					<div class="col-sm-6"> 
					
				
		                
						<b>Descripcion</b>
						<br />
						
						<textarea class="form-control" name="descripcion" placeholder="Descripcion" rows="1">{!!Input::old('descripcion', isset($post)?$post->descripcion:null)!!}</textarea>

						<br/>
						<div>

							<input type="submit" class="btn btn-success" 
							value="{{isset($post)?'Editar':'Agregar'}} producto">
						</div>
					</div>
					<div class="col-sm-6">
						<br/>
						<?php $imgURL=Input::old('image', isset($post)?$post->image:null); ?>
						@include('plugins/subirImagen')
					</div>
				</div>

				

				</form>

			<br/> &nbsp;&nbsp;<br/>&nbsp;&nbsp; <br/>
		</div>
	</div>

@stop