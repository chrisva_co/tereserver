@extends('layouts.base-iframe');

@section('title')
Sobre Nosotros
@stop
@section('body')

<div class="container fixedTop-inverse">
	<br />
	<h1 class="x-center">{!!Lang::get('web.about_title')!!}</h1>

	<br />

	<div class="col-md-12">
		<p class="acerca_de_p">
			{!!Lang::get('web.about')!!}
		</p>
	</div>

	<div class="x-center"><br />

		&nbsp;
		<div class="fb-page" data-href="https://www.facebook.com/{{env('FANPAGE_ID')}}" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/facebook"><a href="https://www.facebook.com/facebook"></a></blockquote></div></div>
	</div>
</div>
@stop