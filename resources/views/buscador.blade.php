@extends('layouts.base-web')

@section('title')
	Resultados de busqueda
@stop


@section('body')

	<div class="container2">
		<div class="container">
			<div class="row">
				<h1 class="menuH1">
					<span class="glyphicon glyphicon-search hidden-xs"></span> Resultados de busqueda</h1>
			</div>
		</div>
	</div>

    <div class="container containerBody">
		<div class="row">

			<div class="col-md-3 col-sm-3">
				<!--include('bloques.fila')-->
			</div>

			<div class=" col-sm-8">
				<script>
				  (function() {
				    var cx = '017458241543287875763:u_73uqx3lpa';
				    var gcse = document.createElement('script');
				    gcse.type = 'text/javascript';
				    gcse.async = true;
				    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
				        '//cse.google.com/cse.js?cx=' + cx;
				    var s = document.getElementsByTagName('script')[0];
				    s.parentNode.insertBefore(gcse, s);
				  })();
				</script>
				<gcse:searchresults-only>
				 	<br />
                    <img class="center" src="{{asset('img/ajax-loader.gif')}}" />
                    <br />

				</gcse:searchresults-only>

			</div>
		</div>	
	</div>		
@stop