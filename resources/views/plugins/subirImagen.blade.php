<?php 
	if(isset($imgURL)) if(!$imgURL) unset($imgURL); 

	function linkImgUrl($imgURL){
		if (stripos($imgURL, 'imgur')!==False)
			return 'href="'.$imgURL.'.jpg"';

		if (stripos($imgURL, 'youtube')!==False)
			return 'href="'.$imgURL.'"';

		return '';
	}

	function vistaPreviaUrl($imgURL){
		if (stripos($imgURL, 'youtube')!==False){
			parse_str( parse_url( $imgURL, PHP_URL_QUERY ), $my_array_of_vars );
			return 'https://i1.ytimg.com/vi/'.$my_array_of_vars['v'].'/hqdefault.jpg'; 
		}
		if (stripos($imgURL, 'imgur')!==False)
			return $imgURL.'s.jpg"';
	}

?>

<style>
	#verImg, #subiendo {display: none; margin-left: 6px;
}
#subiendo {
margin-top: -10px;
}
#previsualizarImg{
	border: 1px solid #B8B8B8;
	border-radius: 4px;
	max-height: 108px;
	max-width: 143px;
	vertical-align: middle;cursor: -webkit-zoom-in; margin-left: 6px;
}
	
#botonSubirUrlx{
	background: #F2F3F8;
	border: 2px solid #D9DBEC;
	color: #4A496D;
	border-radius: 4px;
	font-size: 16px;
	font-weight: bold;
	float: left;
	padding: 5px 10px;
	cursor: pointer;
}
.btn-top{
	vertical-align: top;
}
#botonSubirUrlx:hover{background: #DDE9FF;}
	
</style>

<div style="text-align: left; width: 310px; display: table;">
	<input id="imgSRC" name="imgSRC" style="visibility: collapse; width: 0px;display: none;" 
	type="file" onchange="subirImgur(this.files[0], true); return false">

	
	<div class="btn-group btn-top">
		<a id="botonSubirUrl" onclick="return pcOUrlImgur();" class="btn btn-danger" rel="tooltip"
		data-placement="bottom" title="Formatos: bmp, jpg, png, gif, pdf<?php if(isset($conVideo)){ echo ', Youtube'; }; ?>">
			Subir imagen
		</a>
		<div class="btn-group">
	      <a onclick="return quitarMedia();" id class="btn btn-warning" rel="tooltip" title="Quitar">
	        <span class="glyphicon glyphicon-remove"></span> 
	      </a>
	    </div>
	</div>

    
  
	<p id="subiendo"><img src="{{asset('img/ajax-loader.gif')}}" /></p>
	<a <?php if(isset($imgURL)){ echo linkImgUrl($imgURL); }?> target="_blank"
		id="verImg" style="<?php if(isset($imgURL)){ echo 'display: inline-block'; }?>" rel="shadowbox" class="fancybox">
		<img <?php if(isset($imgURL)){ echo 'src="'.vistaPreviaUrl($imgURL).'"'; }?> id="previsualizarImg" /></a>

	<input type="hidden" id="imagen" name="imagen" value="<?php if(isset($imgURL)){ echo $imgURL; }?>"> <!--redimensionado maximo, aver si me acuerdo en el futuro -->
	<input type="hidden" id="imagenReal" name="image" value="<?php if(isset($imgURL)){ echo $imgURL; }?>"> <!--tamaño real -->
	<input type="hidden" id="imagen160" name="imagen160" value="<?php if(isset($imgURL)){ echo $imgURL; }?>"> <!--tamaño 160x160 centrado -->
</div>


<!-- So here is the magic -->
<script>
	var conVideo={{$conVideo or 'false'}};
	var msgVideo=(conVideo)?'o video de Youtube':'';
	
	
	function pcOUrlImgur(){
		
		bootbox.dialog({
		message: "Selecciona la ubicación del archivo multimedia:",
	  	title: "Subir imagen",
		  buttons: {
		    success: {
		      label: "Mi PC",
		      className: "btn-success",
		      callback: function() {
		        document.querySelector("#imgSRC").click();
		      }
		    },
		    danger: {
		      label: "Internet URL",
		      className: "btn-info",
		      callback: function() {
		        bootbox.prompt('Ingresa enlace url de la imagen'+msgVideo+' <br> <span class="text-muted n">ej: '+
					'http://paginaweb2.com/imagen.jpg<\/span>', function(urlur) {                
				  subirImgur(urlur, false);
				});
		      }
		    },
		    main: {
		      label: "Cancelar",
		      className: "btn-default",
		      callback: function() {
		        
		      }
		    }
		  }
		});
		return false;
	}

    /* Drag'n drop stuff */
    window.ondragover = function(e) {e.preventDefault()}
    window.ondrop = function(e) {e.preventDefault(); upload(e.dataTransfer.files[0]); }

    function quitarMedia(){
    	document.querySelector("#imagen").value = '';
		document.querySelector("#imagenReal").value = '';
		document.querySelector("#previsualizarImg").src = '';

    }
    var data;

		function subirImgur(file, dePC){
			if (!file) return;
			//quito los iframes
		    document.querySelector("#verImg").classList.remove("iframeRep");
			document.querySelector("#verImg").classList.remove("fancybox.iframe");
			mostrarOcultar("subiendo", 1); /* Muestro Subiendo */
			mostrarOcultar("verImg", 0);
			
			if (dePC){
				if (!file.type.match(/image.*/) && !file.type.match(/pdf.*/)){
		       			//alert("Solo se admiten imagenes "+msgVideo+" y archivos PDF");
		       			bootbox.alert("Solo se admiten imagenes "+msgVideo+" y archivos PDF");
		       			return;
		       	}else{
					var reader = new FileReader();
					reader.onload = function(e) {
					    data = e.target.result.substr(e.target.result.indexOf(",") + 1, e.target.result.length);
					    return upload(data, 'base64');
					};
					return reader.readAsDataURL(file);
				}
			}else{
				return upload(file, ''); //de internet
			}

			return false;
		}

		
		function upload(data, tipo){

	 	    $.ajax({
		         url: 'https://api.imgur.com/3/image',
		         headers: {
		            'Authorization': 'Client-ID 5df731ec9eac821'
		         },
		         type: 'POST',
		         data: {
		             'image': data,
		             'type': tipo
		         },
		         success: function(response) {
					imgVieja=document.querySelector("#imagenReal").value.replace(/^.*[\\\/]/, '');

					link="http://i.imgur.com/"+response.data.id;

					document.querySelector("#imagen").value = link+"l.jpg";
					document.querySelector("#verImg").href = response.data.link;
					document.querySelector("#imagenReal").value = link;
					document.querySelector("#imagen160").value = link+"b.jpg";
					document.querySelector("#previsualizarImg").src = link+"s.jpg";

					//agregar al servidor usando ajax*/
					archivo = link.replace(/^.*[\\\/]/, '');
					$("#basura").load('{{asset("mi/img")}}'+'/'+archivo+'/'+imgVieja);
					/*oculto subiendo y muestro subido*/ 
					mostrarOcultar("subiendo",0);
					mostrarOcultar("verImg", 1);
					return false;
		         }, 

				error: function() {
			        tubeId=youtube_ID(data);
					if (conVideo && tubeId!=0){
						document.querySelector("#verImg").href = 'https://www.youtube.com/watch?v='+tubeId;
						//document.querySelector("#verImg").classList.add("iframeRep");
						document.querySelector("#verImg").classList.add("nivo-lbox");			
				       	document.querySelector("#imagen").value = '/img/play_video.jpg';
				       	document.querySelector("#imagenReal").value = 'https://www.youtube.com/watch?v='+tubeId;
				        document.querySelector("#previsualizarImg").src = 'https://i1.ytimg.com/vi/'+tubeId+'/hqdefault.jpg';
					}else{
						//alert("Solo se admiten Formatos: bmp, jpg, png, gif, pdf"+msgVideo);
						bootbox.alert("Solo se admiten Formatos: bmp, jpg, png, gif, pdf"+msgVideo);
					}
					mostrarOcultar("subiendo",0);
					mostrarOcultar("verImg", 1);
					return false;
			    }
		    });
			return false;
		}

	function mostrarOcultar(capa, mostrarOcultar){
		div = document.getElementById(capa);
		if (mostrarOcultar==1)
			div.style.display = "inline-block";
		else
			div.style.display = "none";
	}

	function youtube_ID(url){
	    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
	    var match = url.match(regExp);
	    if (match&&match[7].length==11){
	        return match[7];
	    }else{
	       return 0;
	    }
	}
</script>