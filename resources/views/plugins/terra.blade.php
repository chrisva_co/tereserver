<meta charset="utf-8">
<title>{!! $url !!}</title>
<style>
	input, button{
		font-size: 20px;
   		margin: 5px;
   		margin-left: 0;
    	margin-right: 0;
	}
	#txtOut{
		width: 700px;
	}
	.bloque{
		float: left;
	    margin: 0 10px 0px 0px;
	    background: #CCC;
	    padding: 5px;
	}
</style>

<div id="tresult" style="display:block; margin-top:50px">{!! $contenido !!}</div>

@if ($id!=-1)
<div style="position: fixed;
    top: 0;
    background: #8E7676;
    width: 100%;
    z-index: 1000;
    border: 1px solid #000;">

    <div class="bloque">
		<input type="text" id="txtPre" value="{!! $url !!}" placeholder="Url a analizar" />
		<input type="submit" onclick="analizarUrl()" value="Analizar" />
	</div>
	<div class="bloque">
		<input type="text" id="txtOut" />
		<button onclick="console.log(getAll())">Extraer</button>
	</div>
</div>


<script src="{{asset('/js/maintools.js')}}"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
	var out;
	var datai = document.getElementById("tresult");


	$(window).on('load', function(){
		console.log("Ejecutando");
		console.log(getAll());
	});

	function analizarUrl(){
		url = Base64.encode(document.getElementById("txtPre").value);
		document.location = "{{asset('/')}}stats/0/xhx"+url;
		
	}

	function getAll(){
		//data = document.getElementsByTagName("body")[0];
		out = getJwPlayer();

		if (!out)
			out = getStreamUp();

		if (!out)
			out = getOtrosIframe();

		if (!out)
			out = getIframeReal();

		if (!out)
			out = getVLCPlayer();

		//$("#tresult").html(out);
		document.getElementById("txtOut").value = out;
		
		return out;
	}

	function getVLCPlayer(){
		return document.getElementsByTagName("embed")[0].getAttribute("target");
	}


	function getJwPlayer(){
		data = datai;
		var idjw;
		var file;
		try{
			idjw = data.getElementsByClassName("jwswf")[0].id;
		}catch(err){
			try{
				idjw = data.getElementsByClassName("jw-swf")[0].id;
			}catch(err){
				return;
			}
		}
		if (idjw!=null){
			jwp = jwplayer(idjw);

			if(jwp.config.levels)
				file = jwp.getPlaylistItem()['levels'][0]['file'];
			else
				file = jwp.getPlaylistItem()['file'];
		}
		if (file==null)
			file = jwp.config['file'];

		if (file!=""){
			iguide = jwp.config['rtmp.securetoken'];
			if (iguide!=null){
				file=file+"&&'rtmp.securetoken':'"+iguide+"'";
			}

			return file;	
		}

	}
	// jwplayer("mediaplayer_Player").getPlaylistItem()['file']
	// jwplayer("mediaplayer_Player").config['rtmp.securetoken']

	//https://streamup.com/crivo232/embeds/video?startMuted=true
	function getStreamUp(){
		data = document;
		try{
			src = data.getElementById("su-ivp").src
			if (src.indexOf("streamup.com")!=-1){
				nodos = src.split("/embeds")[0].split("/");
				return src; //porque su m3u8 es muy variado
				id = nodos[nodos.length-1]
				//return 'http://streamup.global.ssl.fastly.net/app/'+id+'s-channel/playlist.m3u8';
				//https://video09-atl.streamup.com/app/innuendotenerife_aac/playlist.m3u8
				//return 'http://video-cdn.streamup.com/app/'+id+'s-stream/playlist.m3u8';
				return 'https://video09-atl.streamup.com/app/'+id+'_aac/playlist.m3u8';
			}
		}catch(err){}
	}
	 

	//http://vaughnlive.tv/embed/video/psntvx22?viewers=true&autoplay=true
	function getOtrosIframe(){
		data = datai;
		arrs = data.getElementsByTagName("iframe");
		var out = new Array();
		for (i=0; i<arrs.length; i++){
			arr = data.getElementsByTagName("iframe")[i];
			if (arr.src.indexOf("vaughnlive.tv")!=-1 || false){
				return arr;
			}
		}
	}


	//toca ponerle mas publicidades, prubas en http://capotv2016.blogspot.com.co/2016/01/liveflash-coronel.html
	function getIframeReal(){
		data = datai;
		arrs = data.getElementsByTagName("iframe");
		iframeSrcConMasWH="";
		var resultadoSUMAWH=0;

		for (i=0; i<arrs.length; i++){
			arr = data.getElementsByTagName("iframe")[i];
			src = arr.src.toLowerCase();
			

			width=arr.style.width.replace("px","");
			if (width=="")
				width=arr.width;

			height=arr.style.height.replace("px","");
			if (height=="")
				height=arr.height;

			suma=(parseInt(width)+parseInt(height));
			
			if (resultadoSUMAWH<suma){
				resultadoSUMAWH=suma;
				iframeSrcConMasWH=src;
			}
		}
		return iframeSrcConMasWH;	
	}

	//Si no existe, saco iframe,
	function getLinkReal(link){
		if (link.indexOf("rivosportt.info")!=-1)
			return link.replace("rivosportt.info","kriv8.ucoz.com")

		if (link.indexOf("rivosportt.info")!=-1)
			return link.replace("rivosportt.info","kriv8.ucoz.com")
	}
</script>
@endif