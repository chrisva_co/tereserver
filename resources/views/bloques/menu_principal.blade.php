<div class=""><!--panel panel-primary-->
				  <div class="panel-heading">
				    <h3 class="panel-title">Menu principal</h3>
				  </div>
				  
				   <div class="list-group">

				   @if(Auth::user()->role==1)
					  <a class="list-group-item" href="{{asset('configuracion')}}">
					  	<span class="glyphicon glyphicon-cog"> </span> General</a>

		              <a class="list-group-item" href="{{asset('productos')}}">
		              	<span class="glyphicon glyphicon-tasks"> </span> Productos</a>

					  <a class="list-group-item" href="{{asset('ubicaciones')}}">
					    <span class="glyphicon glyphicon-map-marker"> </span> Ubicaciones</a>

		              <!--<a class="list-group-item" href="{{asset('promociones')}}"> 
		              <span class="glyphicon glyphicon-usd"> </span> Promociones</a>-->

		              <a class="list-group-item" href="{{asset('usuarios')}}">
		              	<span class="glyphicon glyphicon-user"> </span> Usuarios</a>
		            @endif

		            <!--  -->
		             <!-- <a class="list-group-item" href="/mensajes">Mensajes a usuarios</a> -->

		              <a class="list-group-item" href="{{asset('facturas')}}">
		              	<span class="glyphicon glyphicon-list-alt"> </span> Facturacion</a>

		             <!-- <a class="list-group-item" href="/estadisticas">Estadisticas</a> -->
		              <!--lo mas vendido, lo mas comprado  -->
					</div>
				</div>
