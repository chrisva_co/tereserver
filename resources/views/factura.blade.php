
@extends('layouts.base')

@section('title')
	Admin
@stop

@section('body') 

	
		<style>
			.fixedTop {
				@if($m) 
					margin: 4px;
				@else
					margin-top:38px;
				@endif
			}

		</style>
	

				<h3>Pedido #{{$factura->id}} 

			@if(!$m or true)
    			@if($factura->pagado)
    			    <span class="label label-success right">PAGADO</span>
    			@elseif($factura->informacionDePago)
    				<span class="label label-primary right">{{$factura->informacionDePago->x_respuesta}}</span>
    			@else
    				<span class="label label-warning right">NO PAGADO</span>
    			@endif
			@endif

				

				 </h3>
				<hr style="clear:both; margin: 10px 0;" />

				

				<b>Fecha: </b>{{$factura->created_at->format('j F Y g:i A')}}<br/><br/>


			@if(!$m)
				<b>Nombre cliente:</b>  {{$factura->user->name}}<br/>
				@if($factura->user->identificacionN)
					<b>Cedula cliente:</b> {{$factura->user->identificacionN}}<br/>
				@endif
				<br/>
			@elseif($factura->pagado && false)
				<div style="text-align: center;"><span class="label label-success">PAGADO</span></div>
			@endif

				<b>Productos</b><br/>

				<table class="table table-striped table-hover ">
				  <thead>
				    <tr>
				      <th>Cant{{($m)?"":"idad"}}</span></th>
				      <th>Descripción</th>
				      <th>Importe</th>
				      <th class="hidden-320">Impuesto</th>
				    </tr>
				  </thead>
				  <tbody>

				 
				  <?php $precioTotal=0; $cantidadTotal=0 ?>

					  @foreach($factura->productos as $id=>$post)
					  <?php $precioTotal+=$post->precio; $cantidadTotal+=$post->cantidad; ?>
						<tr>					
						   	<td>{{$post->cantidad}}</td>
						   	<td>{{$post->nombre}}</td>
						   	<td>{{number_format($post->precio)}} {{($m)?"":"COP"}}</td>
						   	<td class="hidden-320">{{($post->iva)?"Iva (".$post->iva."%)":"Sin Iva"}}</td>
						</tr>
					  @endforeach
				  </tbody>

				</table> 
				<hr/>
				<b>Total :</b> {{number_format($precioTotal)}} {{$config->moneda}}<br/>
				
				<b>Estado: </b> {!!($factura->pagado)?'Pagado':'Pendiente de pago'!!}
				<br/>
<br/>
		
			<div style="text-align: center;">
			<?php /*
				<form id="frm_botonePayco" name="frm_botonePayco" method="post" action="https://secure.payco.co/checkout.php" target="_blank"> 
				    <input name="p_cust_id_cliente" type="hidden" value="14879">
				    <input name="p_key" type="hidden" value="58da9539d97404165b17a4ac6b7eae2914159bd4">
				    <input name="p_id_invoice" type="hidden" value="">
				    <input name="p_description" type="hidden" value="{{$cantidadTotal}} articulos de {{$config->almacen}}">
				    <input name="p_currency_code" type="hidden" value="{{$config->moneda}}">
				    <input name="p_amount" id="p_amount" type="hidden" value="{{$precioTotal}}.00">
				    <input name="p_tax" id="p_tax" type="hidden" value="0">
				    <input name="p_amount_base" id="p_amount_base" type="hidden" value="0">
				    <input name="p_test_request" type="hidden" value="FALSE">
				    <input name="p_url_response" type="hidden" value="http://tere.paginaweb2.com/respuestacliente.php"> 
				    <input name="p_url_confirmation" type="hidden" value="http://tere.paginaweb2.com/confirmacionpago.php"> 
				    <input name="p_signature" type="hidden" id="signature"  value="cf6b1222191d943231e9cd79adfb989d" />
				    <input name="idboton" type="hidden" id="idboton"  value="3909" />  
				    <input type="image" id="imagen" src="https://369969691f476073508a-60bf0867add971908d4f26a64519c2aa.ssl.cf5.rackcdn.com/btns/boton_carro_de_compras_epayco6.png" />
				</form> 
			*/ ?>

	


		@if(!$factura->pagado && $precioTotal)
			@if($config->pruebas)
				<form method="post" > <!--target="_blank" -->
					<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

					<button type="submit" class="btn btn-danger">
						<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>
					Pagar ahora</button>
				</form>
				<br/>
			@endif

            @if($factura->informacionDePago)
    				<div>Esta fatura se encuentra en estado <b>{{$factura->informacionDePago->x_respuesta}}</b></div>
    		@else
                <!--491d6a0b6e992cf924edd8d3d088aff1-->
				<form>
					<script
					    src="https://checkout.epayco.co/checkout.js"
					    class="epayco-button"
					    data-epayco-key="69ce28fb69afdf90bdbae1dc9f55edc7"
					    data-epayco-amount="{{$precioTotal}}.00"
					    data-epayco-name="Factura #{{$factura->id}}"
					    data-epayco-description="{{$cantidadTotal}} articulos de {{$config->almacen}}"
					    data-epayco-currency="cop"
					    data-epayco-country="co"
					    data-epayco-test="{{($config->pruebas)?'true':'false'}}"
					    data-epayco-external="{{($m)?'true':'false'}}"
					    data-epayco-response="{{asset('factura/'.$factura->id.'/epayco')}}"
					    data-epayco-confirmation="{{asset('factura/'.$factura->id.'/epayco')}}"
					    data-epayco-button="https://369969691f476073508a-60bf0867add971908d4f26a64519c2aa.ssl.cf5.rackcdn.com/btns/boton_carro_de_compras_epayco6.png"
					    >
					</script>
				</form>
    		@endif
    			

		@endif
	
	<!--
<br><script>suma = (a,b) => a+b</script>
<a class="btn btn-default" onclick="alert('r:'+suma(2,3))">Sumar</a>-->

				<br/>
			@if($m)
		    	[<a href="{{asset('salir')}}">salir</a>]
			@endif

			</div>
@stop
