@extends('layouts.base')

@section('title')
Mapa del sitio

@stop

@section('css')
<style>
    body{
            background: #4a4a4a;
    }
    #mapa{
        margin: auto;
        position: initial;
        position: absolute;
        top: 50%;
    /* left: 50%; */
        margin-top: -{!! ($ubicacion->sizeY)/2 !!}px; 
    /*  margin-left: -{!! ($ubicacion->sizeX)/2 !!}px; */ 
    }
    #tituloCont{
        padding-right: 0px;
        margin-right: -3px;
        padding-left: 0;
    }
    
    
    .objeto .gCabeza button {
        position: absolute;
        top: 1px;
        right: 0;
    }    
    
    .objeto .gCabeza button:last-child {
        position: absolute;
        top: 1px;
        right: 20px;
    }
    
   /*para seleccionar gondola desde categoria*/
   @if($categoria)
	.gondola button[title="{{$categoria}}"], .gondola button[data-original-title="{{$categoria}}"] {
	    animation: blink-animation 1s linear infinite;
        -webkit-animation: blink-animation 1s linear infinite;
	}
	
    @keyframes blink-animation {
      50% {
        background: red;
      }
    }
    @-webkit-keyframes blink-animation {
      50% {
        background: red;
      }
    }
    
	@endif
	.modoVistaPrevia{ display:none !important; }
</style>
@stop

@section('js')
    <script>
        $(function () {
           $('button.gb').tooltip()
        })
    </script>
@stop
@section('body') 
<!--<div style="text-align: center;background: #fff;padding: 5px;">-->
    <div id="mapa" style="width:{!! $ubicacion->sizeX !!}px; height:{!! $ubicacion->sizeY !!}px;"> {!! $ubicacion->mapa !!}</div>
<!--</div>-->
@stop