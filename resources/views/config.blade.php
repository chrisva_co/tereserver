@extends('layouts.base-web')

@section('title')
Admin
@stop

@section('js')
    <script src="{{asset('/js/maintools.js?v=1')}}"></script>

    <script>
        function getFormData($form){
            
            var unindexed_array = $form.serializeArray();
            var indexed_array = {};
        
            $.map(unindexed_array, function(n, i){
                if (n['name']=='categorias')
                    indexed_array[n['name']] = '["'+n['value'].replaceAll(`, `,`,`).replaceAll(` ,`,`,`).replaceAll(`,`,`","`).trim()+'"]';
                else
                    indexed_array[n['name']] = n['value'];
            });
            return indexed_array;
        }
        
            $('#btnActualizar').on('click', function(){
    			$.post( "{{asset('/configuracion')}}", {_token: "{{ csrf_token() }}", id: 1, descripcion: JSON.stringify(getFormData($("#frmConfig"))) })
    			  .done(function( data ) {
    			    bootbox.alert( data );
    			});
            });
        
    </script>
@stop

@section('body') 
	<div class="container">
		<div class="row">
			<div class="col-sm-2">

	            

				@include('bloques/menu_principal')

			</div>

			<form method="post" class="frmUnico" id="frmConfig">

				<div class="col-sm-10">
					<h2>Configuracion general</h2>
					<hr/>
						@foreach($config as $pos=>$conf)
    					     <div class="form-group"><label>{{$pos}}</label>
    					     @if ($pos!="categorias")
    		                    {!! Form::text($pos,  Input::old($pos, $config[$pos]) , ['class'=>'form-control', 'placeholder'=>$pos]) !!}
    		                  @else
    		                    <textarea class="form-control" name="categorias" id="categorias" placeholder="Categorias" rows="5">{!! implode(', ',$conf) !!}</textarea>
    		                  @endif
    		                </div>
    		            @endforeach
    		            
					
					<br />
					<span id="btnActualizar" class="btn btn-success">Actualizar</span>
				</div>
			</form>
		</div>


	</div>
@stop