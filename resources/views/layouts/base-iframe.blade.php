@extends('layouts.base')

@section('js')
@stop

@section('css')
@stop

@section('body')
    


	<div id="cuerpoIf" class="container fixedTop-inverse">
		<form method="post" onsubmit="return fsubmit()">
			{!! Form::token() !!}
			<h3>@yield('titulo')</h3>

			<div class="row">
				<div class="col-xs-12" style="
    text-align: right;
">
					<button type="submit" id="btn-iframe" class="btn btn-success "><span class="glyphicon glyphicon-save"></span> Guardar <span class="hidden-xs">cambios</span></button>
				
					<a href="javascript:parent.jQuery.fancybox.close();" class="btn btn-primary ">
						<span class="glyphicon glyphicon-remove"></span>
					</a>
				</div>
			</div>
			<hr class="dashed">

			@yield('contenido')


		</form>
	</div>
@stop