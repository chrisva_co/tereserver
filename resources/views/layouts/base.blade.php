<!DOCTYPE html>

<html lang="es">
  <head>
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    @if(!isset($_GET['sinvp']))
        <meta name="viewport" content="width=device-width, initial-scale=1">
    @else
    @endif
    <meta name="description" content="@yield('descripcion')">
    <meta name="keywords" content="@yield('keywords')">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}">
    <link rel="image_src" href="@yield('imagen', asset('img/envivo.png'))"/>

    <meta name="theme-color" content="#3C3734"/>
     <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#3C3734"/>
    

    <title>@yield('title') « {{env('SITEURL')}}</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">


    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" >

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" >
    <link rel="stylesheet" href="https://select2.github.io/select2-bootstrap-theme/css/select2-bootstrap.css" >
    
    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}?n=2" >
    <link rel="stylesheet" href="{{asset('plugins/fancybox/jquery.fancybox.css')}}" >

    @yield('css')
  </head>

  <body>
    <script>
    //window.navigator.__defineGetter__('userAgent', function () {
      //return 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10) AppleWebKit/537.16 (KHTML, like Gecko) Version/8.0 Safari/537.16';
  //});
    </script>

    <!--MAIN TITLE-->
  <div id="fb-root"></div>
    @yield('header')
    @include('plugins.notificacions')

    <!--<div class="@if(!Session::get('error')) fixedTop @endif">-->
     <div class="@if(!isset($_GET['iframe-base'])) fixedTop @endif">

        @yield('body')

         @yield('footer')
    </div>

<?php //echo $_SERVER['HTTP_USER_AGENT'] ?>
  <div id="basura">
  </div>

             <script>
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

              ga('create', 'UA-53198140-2', 'auto');
              ga('send', 'pageview');

            </script>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="{{asset('js/bootbox.min.js')}}"></script>
    <script src="{{asset('plugins/fancybox/jquery.fancybox.js')}}"></script>
    <script src="{{asset('js/iframe-auto-height.js')}}"></script>
    <script src="//maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
    <script src="{{asset('js/jquery.geocomplete.min.js')}}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <script src="{{asset('js/web.js')}}"></script>

    @yield('js')
 
  </body>
</html>
