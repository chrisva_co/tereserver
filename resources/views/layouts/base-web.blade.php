@extends('layouts.base')

@section('header') 

<nav class="navbar-default navbar navbar-fixed-top" role="navigation">

  <div class="logo_center"> 
      <a href="{{asset('/')}}">Tere Assistant
       <!--<img src="{{asset('img/logo.png')}}" class="logo_img" alt="{!!Lang::get('web.slogan')!!}" title="directo.in" rel="tooltip" data-placement="bottom">--></a>
  </div>

<div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only"> x </span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>


    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

      <ul class="nav navbar-nav navbar-right">

    @if(!Auth::check())
                  
          <!--<li><a id="botonFb" href="{{asset('auth/facebook')}}">
            <span class="glyphicon glyphicon-user"></span> {{Lang::get('web.entrar_con_facebook')}}</a></li>-->

        <!-- <li><a id="botonGl" href="{{asset('auth/google')}}">
            <span class="glyphicon glyphicon-user"></span> Entrar con google</a></li> -->


        <li class="active hidden-sm"></li>
       


        @else
        <li class="hidden-xs">
          <img class="avatarMini" src="{{Auth::user()->avatar}}">
        </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              {{Auth::user()->name}} <b class="caret"></b></a>
            <ul class="dropdown-menu">
               
            <li><a href="{{asset('salir')}}">{{Lang::get('web.logout')}} <span class="glyphicon glyphicon glyphicon-log-out"></span> </a></li>
          </ul>
          </li>              

        @endif
        <li class="dropdown active">

           <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
             <span class="glyphicon glyphicon-education"></span></a>

          <ul class="dropdown-menu" role="menu">
            <li><a data-fancybox-type="iframe" href="http://usc.edu.co" class="" parent="_blank">Ir a USC</a></li>
          </ul>
        </li>



      </ul>
    </div>
  </div>
    </nav>
@stop

@section('footer') 
<br>
<div class="container">
  <div class="">
    <center class="hidden-xs">

    </center>
   
  </div>
</div>

    <div style="float:left; height:0px; width: 100%; overflow:hidden">
         

    <a href="https://whos.amung.us/stats/yf6vxackph3f/">
      <img src="https://whos.amung.us/widget/yf6vxackph3f.png" border="0" style="height:0px;"></a>

  </div>
@stop