@extends('layouts.base-web')

@section('title')
	Admin - Facturas
@stop

@section('js')
	<script src="{{asset('/js/maintools.js')}}"></script>
	<script src="{{asset('/js/iframe-auto-height.js')}}"></script>

	<script type="text/javascript">

		function mostrarDetalles(id){

				url="{!!asset('factura/')!!}/";
				$("#listaEM").html('<iframe class="iframe" src="'+url+id+'" width="100%" ></iframe>');
				$('.iframe').iframeAutoHeight({debug: true});

			$( "#listaEM" ).fadeOut(1,function(){
				$("#listaEM").fadeIn();
			});
			$( "#listaEM" ).fadeOut(1,function(){
				$("#listaEM").fadeIn();
			});


			
		}
	</script>
@stop

@section('body') 
	<div class="container">
		<div class="row">
			<div class="col-sm-2">
				@include('bloques/menu_principal')

			</div>


			<div class="col-sm-5">
				<h2>Facturas
				<!--<a data-fancybox-type="iframe" href="producto" class="fancybox-iframe-no-modal btn btn-info right">Añadir un producto</a>--></h2>
				
				
				<hr/>

				<table class="table table-striped table-hover ">
				  <thead>
				    <tr>
				      <th>Fecha</th>
				      <th>Total</th>
				      <th>Estado</th>
				      <th>Detalles</th>
				    </tr>
				  </thead>
				  <tbody>

				  

				    @foreach($facturas as $id=>$post)
						<tr onclick="mostrarDetalles('{{$post->id}}')">					
					     	<td>{{$post->created_at->format('j F Y')}}</td>
					      	<td>{{($post->getCantidadPrecio()['valorTotal'])." ".$config->moneda}}</td>
					      	<td>

					      	{!!($post->pagado)?'<span class="glyphicon glyphicon-ok text-success"> </span>':'<span class="glyphicon glyphicon-warning-sign text-warning"> </span>'!!}

					      	</td>
					      
					      	<td>

								<a class="btn btn-info btn-xs inline" onclick="mostrarDetalles('{{$post->id}}')">Detalles</a>

					      	</td>
					    </tr>
					@endforeach
				      
				   
				  </tbody>
				</table> 

				@if(count($facturas))

					<div style="text-align:center">
						{!! $facturas->render() !!}
					</div>

				@endif
				
			</div>

			<div class="col-sm-5" id="listaEM">

			<h3 class="web-h2"><span class="glyphicon glyphicon-hand-left blink"></span> Selecciona una factura</h3>
			</div>
		</div>

	</div>
@stop