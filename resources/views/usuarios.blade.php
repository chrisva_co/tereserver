@extends('layouts.base-web')

@section('title')
Admin - Productos
@stop

@section('body') 
	<div class="container">
		<div class="row">
			<div class="col-sm-2">

	            

				@include('bloques/menu_principal')

			</div>


			<div class="col-sm-10">
				<h2>Usuarios registrados
				<!--<a data-fancybox-type="iframe" href="producto" class="fancybox-iframe-no-modal btn btn-info right">Añadir un producto</a>--></h2>
				
				<br>
				<span class="label label-warning">Total: <b>{{$total}}</b></span>

				<hr/>




<table class="table table-striped table-hover ">
  <thead>
    <tr>
      <th>id</th>
      <th>Nombre</th>
      <th>Email</th>
      <th>Rol</th>
      <th>Direccion</th>
      <th>telefono</th>
      <th>Compras</th>
      <th>Avatar</th>
    </tr>
  </thead>
  <tbody>

  

    @foreach($usuarios as $id=>$post)
		<tr>					
			<td>{{$post->id}}</td>
	     	<td>{{$post->name}}</td>
	      	<td>{{$post->email}}</td>
	      	<td>{{$post->role()}}</td>
	      	<td>{{$post->direccion}}</td>
	      	<td>{{$post->telefono}}</td>
	      	<td>{{$post->facturas->count()}}</td>
	      	<td>

				<a href="{!!$post->avatar!!}&fbx=.jpg" target="_blank" id="verImg" style="display: inline-block" class="fancybox">
					<img src="{!!$post->avatar!!}"  width="20" id="previsualizarImg">
				</a>

	      	</td>
	    </tr>
	@endforeach
      
   
  </tbody>
</table> 

			@if(count($usuarios))

					<div style="text-align:center">
						{!! $usuarios->render() !!}
					</div>

				@endif
				
					
			</div>
		</div>

	</div>
@stop