@extends('layouts.base-web')

@section('title')
Admin - Ubicacion
@stop

@section('css')
<style>
    #mapa{
        width: 780px;
        height: 480px;
        margin: auto;
    }
    #tituloCont{
        padding-right: 0px;
        margin-right: -3px;
        padding-left: 0;
    }
    
    
    .objeto .gCabeza button {
        position: absolute;
        top: 1px;
        right: 0;
    }    
    
    .objeto .gCabeza button:last-child {
        position: absolute;
        top: 1px;
        right: 20px;
    }
    
   /*para seleccionar gondola desde categoria
	.gondola button[title="latios"] {
	    background: #ffeb00 !important;
	}*/
</style>
@stop

@section('js')
    
    <script src="{{asset('js/MooTools-More-1.6.0-compat-compressed.js')}}"></script>
    
    <script>
		function MayPrimera(string){
		  return string.charAt(0).toUpperCase() + string.slice(1);
		}


		function ClassObjeto(id, nombre, tipo, width, height, top, left, grados=0){
			//Atributos
			this.id = id;
			this.tipo = tipo;
			this.nombre = nombre;
			this.width = width;
			this.height = height;
			this.top = top;
			this.left = left;
			this.grados = grados;
		}

		//
		function ClassGondola(nombre, width, height, top, left, grados=0, bloques="ABCDE"){
			//Atributos
			ClassObjeto.call(this, -1, nombre, "gondola", width, height, top, left, grados);
			this.bloques=bloques;
			//categoria de bloques
			this.a = "";	this.c1 = "";	this.c2 = "";	this.d1 = "";
			this.d2 = "";	this.e1 = "";	this.e2 = "";	this.b = "";
			//this.bloquesVisible=new Array();
			//Metodos
			this.refrescar = function(){
			}
		}


		function Caja(nombre="Cajas", width, height, top, left, grados=0){
			ClassObjeto.call(this, -1, nombre, "caja", width, height, top, left, grados);
		}


		function Etiqueta(nombre, width, height, top, left, grados=0){
			ClassObjeto.call(this, -1, nombre, "etiqueta", width, height, top, left, grados);
		}

		function Mapa(nombre, width, height, top, left, imageFondo=""){
			ClassObjeto.call(this, -1, nombre, "mapa", width, height, top, left, 0);
			this.imageFondo = imageFondo;
			this.elementos=new Array();
		}


		//asd.children[0].attributes.class
		function botonClick(elemento){
			nombre = elemento.title;

			if (nombre == "Girar"){
				if ($( "#girosEn" ).val()==0){
					giro="";
				}else{
					girosEn=$( "#girosEn" ).val();
					if ($('#giroNegativo').prop("checked"))
						girosEn=-$( "#girosEn" ).val();
					e = Number(elemento.parentElement.parentElement.style.transform.replace(/[a-zA-Z \(\)]/g, ''))+Number(girosEn);
					giro="rotate("+e+"deg)";
				}
			
				elemento.parentElement.parentElement.style.transform = giro;

			}else if(nombre == "Editar"){
				id = elemento.parentElement.parentElement.id;
				tipo = elemento.parentElement.parentElement.attributes.class.value;

				nombre = elemento.parentElement.children[0].innerText;
				ancho = elemento.parentElement.parentElement.style.width.replace(/[a-zA-Z \(\)]/g, '');
				if (!ancho)
					ancho = $(elemento.parentElement.parentElement).width();


				if (tipo.indexOf("gondola")==-1){ //si no es una gondola
					alto = elemento.parentElement.parentElement.style.height.replace(/[a-zA-Z \(\)]/g, '');
					if (!alto)
						alto = $(elemento.parentElement.parentElement).height();
				}else{
					alto = elemento.parentElement.parentElement.getElement(".gbC").style.height.replace(/[a-zA-Z \(\)]/g, '');
					if (!alto)
						alto = $(elemento.parentElement.parentElement.getElement(".gbC")).height(); //children[1].children[1].children[0]
					bloques = elemento.parentElement.parentElement.attributes.rel.value;
				}
			
				//bloques = "ABCE";

				msg="<b>Editar</b><br/>";
				msg+="<div class='row'>";
				msg+="<div class='col-xs-3'>Nombre<input type='text' id='nombreEtiqueta' class='form-control' value='"+nombre+"' placeholder='Nombre de la etiqueta' /></div>";
				msg+="<div class='col-xs-2'>Ancho<input type='number' id='ancho' step='1' class='form-control' placeholder='Ancho' value='"+ancho+"' /></div>";
				msg+="<div class='col-xs-2'>Alto<input type='number' id='alto' step='1' class='form-control' placeholder='Alto' value='"+alto+"' /></div>";
			if (tipo.indexOf("gondola")==-1){
				msg+="<div class='col-xs-3'>Tipo<select id='tipo' class='form-control' placeholder='Selecciona categoria' >";
				msg+="<option value='baño'>Baño</option> <option value='caja'>Caja pago</option> <option value='etiqueta'>Etiqueta</option> <option value='etiqueta sin borde'>Etiqueta sin borde</option></select></div>";	
			
				msg+="<script>";
				msg+="$(`#tipo option[value='"+elemento.parentElement.parentElement.getAttribute(`title`)+"']`).attr('selected',true);";
				msg+="<\/script>";
			}else{		
				msg+="<div class='col-xs-3'>Visibles<input type='text' id='bloques' class='form-control' title='Cabinas visibles' value='"+bloques+"' /></div>";
			}			
				msg+="<div class='col-xs-2'><br/> <button class='btn btn-danger btn-block' onclick='$(`.bootbox-close-button`).click(); eliminarElemento(`"+id+"`)' title='Eliminar elemento'> <i class='glyphicon glyphicon-remove'></i> </button></div>";
				msg+="</div>";
				bootbox.confirm(msg,function(d){ 
					if (d){
						if (tipo.indexOf("gondola")==-1){
							elemento.parentElement.parentElement.title = $( "#tipo" ).val();
							elemento.parentElement.parentElement.style.height = $( "#alto" ).val()+'px';
						}else{
							elemento.parentElement.parentElement.rel = $( "#bloques" ).val();
							elemento.parentElement.parentElement.getElements(".gbC").forEach(function(element) {
					  		element.style.height=$( "#alto" ).val()+'px'; });
							elemento.parentElement.parentElement.attributes.rel.value = $( "#bloques" ).val();
						}
						elemento.parentElement.children[0].innerText = $( "#nombreEtiqueta" ).val();
						elemento.parentElement.parentElement.style.width = $( "#ancho" ).val()+'px';
					}
				});
			}
		}

		function botonCategoriaClick(boton){
			msg="<b>Categoria</b><br/>";
			msg+="<div class='row'>";
			msg+="<div class='col-xs-9'>";
			//msg+="<select id='categoria' class='form-control' placeholder='Selecciona categoria' ><option value=''>Selecciona</option> <option value='latios'>Lactios</option> <option value='verduras'>Verduras</option>";
		    msg+=`{!! Form::select('categoria', $config->categorias, null,['id'=>'categoria', 'class'=>'form-control', 'placeholder'=>'Selecciona categoria']) !!}`;
			msg+="</div></div>";
            
			msg+="<script>";
			msg+="$(`#categoria option[value='"+$(boton).attr(`title`)+"']`).attr('selected',true);";
			msg+="<\/script>";
			bootbox.confirm(msg,function(d){ 
				if (d){
					$(boton).attr('title', $( "#categoria" ).val()); 
				}
			});
		}

		function eliminarElemento(id){
			//al eliminar, se le suma el top a los elementos que estaban anterior a este {caso exito}
			document.getElement("#"+id).remove();
		}




		gondolaVista= function(totalTop){ return `<div class="gondola objeto" id="`+Date.now()+`" title="gondola"  style="top:-`+totalTop+`px" rel="ABC">
			<div class="gCabeza">
				<span class=""></span> 
				<button class="btn btn-xxxs btn-info right modoVistaPrevia editar" data-toggle="tooltip" data-placement="top" title="Editar"><i class="glyphicon glyphicon-edit"></i></button>
				<button class="btn btn-xxxs right modoVistaPrevia girar" data-toggle="tooltip" data-placement="top" title="Girar"><i class="glyphicon glyphicon-repeat"></i></button>
			</div>

			<div class="gCuerpo">
				<button class="btn btn-xs btn-default gb gBA" title="">a</button>
				<div class="gCenter gBC">
					<button class="btn btn-xs btn-default gb gbC gbL" title="">c1</button>
					<button class="btn btn-xs btn-default gb gbC gbR" title="">c2</button>
				</div>
				<div class="gCenter gBD">
					<button class="btn btn-xs btn-default gb gbC gbL" title="">d1</button>
					<button class="btn btn-xs btn-default gb gbC gbR" title="">d2</button>
				</div>
				<div class="gCenter gBE">
					<button class="btn btn-xs btn-default gb gbC gbL" title="">e1</button>
					<button class="btn btn-xs btn-default gb gbC gbR" title="">e2</button>
				</div>

				<button class="btn btn-xs btn-default gb gBB" title="">b</button>
			</div>
		</div>`;}
	




		objetoVista = function(tipo, totalTop){
				return `<div class="`+tipo+` objeto" title="`+tipo+`" style="top:-`+totalTop+`px" id="`+Date.now()+`">
			<div class="gCabeza">
					<span class="titulo">`+MayPrimera(tipo)+`</span>  
					<button class="btn btn-xxxs btn-info right modoVistaPrevia editar"  data-toggle="tooltip" data-placement="top" title="Editar"><i class="glyphicon glyphicon-edit"></i></button>
					<button class="btn btn-xxxs right modoVistaPrevia girar" data-toggle="tooltip" data-placement="top" title="Girar"><i class="glyphicon glyphicon-repeat"></i></button>
				</div>
				<div class="gCuerpo">
					<i class="glyphicon glyphicon-shopping-cart icaja"></i>
					<img src="https://i.imgur.com/dqWZGFX.png" class="iconMapa ibaño" />

				</div>
			</div>`
		}


		function agregarObjetoMapa(tipo){

			totalTop = 0;
			document.getElements('.objeto').forEach(function(elemento){ return totalTop+=$(elemento).height() });

			//agrego objeto
			if (tipo == 'gondola')
				$('#mapa').append(gondolaVista(totalTop));
			else 
				$('#mapa').append(objetoVista(tipo, totalTop));
		

			//le doy movilidad
			new Drag.Move(document.getElements('.objeto').getLast(),{
			   container: document.getElement('#mapa')
			});
		}

		$( document ).on( "click", ".gb", function() {
			botonCategoriaClick(this);
		});


		$( document ).on( "click", ".gCabeza button", function() {
			botonClick(this);
		});

		$("#sizeX").change(function() {
			document.getElement("#mapa").style.width = $("#sizeX").val()+'px';
		});
		$("#sizeY").change(function() {
			document.getElement("#mapa").style.height = $("#sizeY").val()+'px';
		});

		document.getElements('.objeto').forEach(function(elemento){    new Drag.Move(elemento,{
		   container: document.getElement('#mapa')
		   });    
		});
        $('#btnGuardar').on('click', function(){
			$.post( "{{asset('/ubicaciones')}}", { _token: "{{ csrf_token() }}", sizeX: $("#sizeX").val(), sizeY: $("#sizeY").val(), mapa: document.getElement("#mapa").innerHTML })
			  .done(function( data ) {
			    bootbox.alert( data );
			});
		});
	</script>
	
@stop

@section('body') 
	<div class="container">
		<div class="row">
			<div class="col-sm-2">


				@include('bloques/menu_principal')

			</div>

            <div class="col-xs-10">

        				
                 <div class="row">
        			<div class="col-sm-10">
                      <div class="panel-heading" id="tituloCont">
            		   <h3 class="panel-title">Mapa del almacen <span class="btn right btn-xs btn-warning" onclick="bootbox.confirm('Estas seguro que deseas eliminar todos los elementos del mapa?', function(x){ if (x) $('#mapa').empty(); })">Vaciar mapa</span></h3>
            		</div>  				
        			
            			<div id="mapa" style="width:{!! $ubicacion->sizeX !!}px; height:{!! $ubicacion->sizeY !!}px;"> {!! $ubicacion->mapa !!}</div>
        			
        
        			</div>
        			
        			<div class="col-sm-2">
        			     <div style ="height: 38px;"></div>
        			     
        			    <span class="btn btn-xs btn-primary btn-block" onclick="agregarObjetoMapa('gondola')">Gondola</span>
		    			<span class="btn btn-xs btn-primary btn-block" onclick="agregarObjetoMapa('baño')">Baño</span>
		    			<span class="btn btn-xs btn-primary btn-block" onclick="agregarObjetoMapa('etiqueta')">Etiqueta</span>
		    			<span class="btn btn-xs btn-primary btn-block" onclick="agregarObjetoMapa('caja')">Caja de pago</span>
<br/>

                <div class="form-group ">
                    <label>Tamaño del mapa </label>
                    <div class="input-group">
                           <span class="input-group-addon"> <i class="glyphicon glyphicon-resize-horizontal"></i></span>
                            {!! Form::select('sizeX', $size[0] , $ubicacion->sizeX, ['class'=>'form-control chosen-select-deselect ', 'tabindex'=>'7','id'=>'sizeX']) !!}
                    </div>
                    
                    <div class="input-group">
                           <span class="input-group-addon"> <i class="glyphicon glyphicon-resize-vertical"></i></span>
                            {!! Form::select('sizeY', $size[1] , $ubicacion->sizeY, ['class'=>'form-control chosen-select-deselect ', 'tabindex'=>'7','id'=>'sizeY']) !!}
                    </div>
                </div>
                
                <div class="form-group " style="overflow:hidden">
                    <label>Giros en </label>

                    <div class="input-group">
                           <span class="input-group-addon"> <i class="glyphicon glyphicon-repeat"></i></span>
                            <select tabindex="7" class="form-control" id="girosEn" >
        				        <option value=5>5°</option><option value=15>15°</option><option value=30>30°</option><option value=45>45°</option><option value=90  selected="selected">90°</option> <option value=0>Reestablecer</option></select>
                            </div>
        		    <div class="checkbox" style=" margin-top: 5px;"><label> <input type="checkbox" id="giroNegativo" /> Girar en Negativo </label></div>
                            
                </div>
                
                
            			<button id="btnGuardar" class="btn btn-xs btn-success btn-block">Guardar</button>
        			</div>
			</div></div>
		</div>

	</div>
@stop