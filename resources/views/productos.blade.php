@extends('layouts.base-web')

@section('title')
Admin - Productos
@stop

@section('body') 
	<div class="container">
		<div class="row">
			<div class="col-sm-2">

	            

				@include('bloques/menu_principal')

			</div>


			<div class="col-sm-10">
				<h2>Productos

					<a data-fancybox-type="iframe" href="producto" class="fancybox-iframe-no-modal btn btn-info right">Añadir un producto</a></h2>
				<a id="btnSincronizarP" class="btn btn-xs btn-warning right">Sincronizar productos</a></h2>

				<br>
				<span class="label label-warning">Total: <b>{{$totalProductos}}</b></span>

				<hr/>




<table class="table table-striped table-hover ">
  <thead>
    <tr>
      <th>id</th>
      <th>Barcode</th>
      <th>Nombre</th>
      <th>Precio</th>
      <th>Categoria</th>
      <th>Iva</th>
       <th>formato</th>
       <th>Ubicacion</th>
      <th>Mas info</th>
    </tr>
  </thead>
  <tbody>
    

    @foreach($productos as $id=>$producto)
		<tr>					
			<td>{{$producto->id}}</td>
	     	<td>{{$producto->barCode}}</td>
	      	<td>{{$producto->nombre}}</td>
	      	<td>{{$producto->valorUnitario}}</td>
	      	<td>{{$producto->categorias()}}</td>
	      	<td>{{$producto->conIva()}}</td>
	      	<td>{{$producto->formato}}</td>
	      	<td>{{$producto->ubicacion}}</td>
	      	<td>
	      		<div class="">
					<a href="{!!asset('producto/'.$producto->id)!!}" class="btn btn-info btn-xs fancybox-iframe-no-modal inline"  data-fancybox-type="iframe"  >Editar</a>

					<a href="#" onclick="bootbox.confirm('Seguro que quiere eliminar?', (x)=>((x)?alert('Eliminado'):null))" class="btn btn-danger btn-xs"  ><span class="glyphicon glyphicon-remove"> </span> </a>

				</div>
			</td>
	    </tr>
	@endforeach
      
   
  </tbody>
</table> 

			@if(count($productos))

					<div style="text-align:center">
						{!! $productos->render() !!}
					</div>

				@endif
					
			</div>
		</div>

	</div>
@stop