<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/pago.php', function(){
    return redirect("/");
});
Route::get('/', 'MainController@getPrincipal');

Route::get('producto/{slug?}', 'MainController@getProducto');
Route::post('producto/{slug?}', 'MainController@postProducto');
Route::get('/productos', 'MainController@getProductos');

Route::get('/usuarios', 'MainController@getUsuarios');
Route::get('/facturas/{usuario_id?}', 'MainController@getFacturas');


Route::get('/factura/{id}/pagado', 'MainController@getProductoPagado');
Route::get('/factura/{id}', 'MainController@getFactura');
Route::post('/factura/{id}', 'MainController@postFactura');
Route::get('/factura/{id}/epayco', 'MainController@getPagoDeEpayco');
Route::post('/factura/{id}/epayco', 'MainController@getPagoDeEpayco');

Route::get('/configuracion', 'MainController@getConfiguracion');
Route::post('/configuracion', 'MainController@postConfiguracion');

Route::get('/ubicacion/{cat?}', 'MainController@getUbicacion');
Route::get('/ubicaciones', 'MainController@getUbicaciones');
Route::post('/ubicaciones', 'MainController@postUbicaciones');


Route::get('/app/productos/{slug?}/{otra?}', 'MainController@getJsonProductos');


Route::get('/app/login', 'MainController@getLoginDesdeApp');
Route::post('/app/buscar', 'MainController@postJsonProductos');
Route::post('/app/escanear', 'MainController@escanearProducto');
Route::get('/app/recomendar/{ids}/{cantidadRecomendaciones?}', 'MainController@getRecomendacion');
Route::post('/app/recomendar/{ids}/{cantidadRecomendaciones?}', 'MainController@getRecomendacion');

//Sobre nosotros
Route::get('about', function () {
    return view('about');
});
//Buscador
Route::get('buscar', function () {
    return view('buscador');
});
//Sitemap
//Route::get('sitemap.xml/{dominio?}', 'CanalController@getSitemap'); 




Route::get('/redirect', 'SocialAuthController@redirect');
Route::get('/callback', 'SocialAuthController@callback');


//Route::controller('cuenta', 'Auth\AuthController');
//Route::get('auth/facebook', 'Auth\SocialAuthController@redirectToProviderFB');
//Route::get('auth/facebook/callback', 'Auth\SocialAuthController@handleProviderCallbackFB');
//Route::get('auth/facebook/principal', 'Auth\SocialAuthController@principal');
//Route::get('auth/google', 'Auth\SocialAuthController@redirectToProviderGL');
//Route::get('auth/google/callback', 'Auth\SocialAuthController@handleProviderCallbackGL');


Route::get('auth/{provider}', 'Auth\SocialAuthController@redirectToProvider')
			->name('niIdeaPaQueEsto');

Route::get('auth/{provider}/callback', 'Auth\SocialAuthController@handleProviderCallback')
		  ->name('niIdeaPaQueLoOtro');



Route::get('salir', function () {
	Auth::logout();
    return redirect()->back();
});


Route::get('chris', function () {
	if(Auth::check())
		return "iniciada";
	return "entrar";
});
