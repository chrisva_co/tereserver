var objUbicacion;
var formatoHora=formatoRegion();



$(document).on('ready', function(){

    //$('.js-example-basic-single').select2(); caso exito
	$('#btnSincronizarP').on('click', sincronizarProductos);

	$(".fancybox-iframe, .fIframe").fancybox({
		maxWidth	: 1200,
		maxHeight	: 700,
		fitToView	: false,
		width		: '100%',
		height		: '100%',
		autoSize	: true,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none',
		padding		: 1,
		modal		: 'true'
	});

	$(".fancybox-iframe-no-modal").fancybox({
		maxWidth	: 900,
		maxHeight	: 500,
		fitToView	: false,
		width		: '80%',
		height		: '97%',
		autoSize	: true,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none',
		padding		: 1
	});

	$(".fancybox").fancybox({
		maxWidth	: 800,
		maxHeight	: 700,
		fitToView	: false,
		width		: '70%',
		height		: '90%',
		autoSize	: true,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none',
		padding		: 1,
	});

	$("input.ubicacion").geocomplete({
		types: ['(cities)']
	});
	showHideLinks();
	getGeo();


	$(document.getElementsByClassName("mediax")).each(function(index, obj){
		try{
			hora=obj.getAttribute("hora");

			horaGMT = convertirAHoraLocal(hora);
			document.getElementsByClassName("bloque-letra-img")[index].innerHTML = horaGMT;

		}catch(err){}
	 });

	$(document.getElementsByClassName("cambiarHora")).each(function(index, obj){
		try{
			hora=obj.getAttribute("hora");

			horaGMT = convertirAHoraLocal(hora);
			document.getElementsByClassName("cambiarHora")[index].innerHTML = horaGMT;

		}catch(err){}
	 });
});


function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

function sincronizarProductos() {

	bootbox.confirm("La plataforma se conectará a los servidores de Mercapava S.A. \nDesea continuar?",
		function(r){
		if (r){
			bootbox.alert("Sincronizando.... ");
			setTimeout(function(){
				$("button.bootbox-close-button.close").click();
				bootbox.alert("Error: 500");
			}, 5000);
		}
	});


}


function convertirAHoraLocal(hora){
	horaGMT = moment(hora+":00.000Z").format(formatoHora);
	if (horaGMT=='Invalid date')
		horaGMT='';
	return horaGMT;
}

function diasEntreFechas(fechaMenor, fechaMayor, sinHoras){
  //si da negativo, es porque fechaMenor es menor que fecha Mayor ej: 2009 2010
  var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
	try{
	  if (sinHoras){

	  	fechaMenor.setHours(0);
		fechaMenor.setMinutes(0);
	  	fechaMenor.setSeconds(0);

		fechaMayor.setHours(0);
		fechaMayor.setMinutes(0);
		fechaMayor.setSeconds(0);

	  }
	  return diffDays = Math.round(((fechaMenor.getTime() - fechaMayor.getTime())/(oneDay)));
	}catch(err){
		return '';
	}
}

function getGeo(){
	/*$.getJSON( "//freegeoip.net/json/", function( data ) {
		objUbicacion = data;


		if (true){
			alert("nulo");
			timezone=moment().format("Z");
		}else{
			timezone=data.time_zone;
		}

		document.getElementById("horaCountry").innerHTML = timezone;
	});*/
	try{
		document.getElementById("horaCountry").innerHTML ="GMT"+moment().format("Z");
	}catch(err){}
}

function formatoRegion(){
	huso=new Date().getTimezoneOffset()*(-1);
	var formato="H:mm";
	if ( (huso == 600) || (huso == 570) || (huso == -300) || (huso
		== -420) || (huso == -480) || (huso == -540) || (huso ==-600)
		|| (huso ==-660) )
		{
			formato="h:mm a";
		}
	return formato;
}


function showHideLinks(){
	$(".linkAjax").click(function() {
		var form = $(this).attr("href");
		$(form).slideToggle('slow');
		return false;
	});
}

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.4&appId=970001699731158";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

