var FlashDetect=new function(){var self=this;self.installed=false;self.raw="";self.major=-1;self.minor=-1;self.revision=-1;self.revisionStr="";var activeXDetectRules=[{"name":"ShockwaveFlash.ShockwaveFlash.7","version":function(obj){return getActiveXVersion(obj);}},{"name":"ShockwaveFlash.ShockwaveFlash.6","version":function(obj){var version="6,0,21";try{obj.AllowScriptAccess="always";version=getActiveXVersion(obj);}catch(err){}
return version;}},{"name":"ShockwaveFlash.ShockwaveFlash","version":function(obj){return getActiveXVersion(obj);}}];var getActiveXVersion=function(activeXObj){var version=-1;try{version=activeXObj.GetVariable("$version");}catch(err){}
return version;};var getActiveXObject=function(name){var obj=-1;try{obj=new ActiveXObject(name);}catch(err){obj={activeXError:true};}
return obj;};var parseActiveXVersion=function(str){var versionArray=str.split(",");return{"raw":str,"major":parseInt(versionArray[0].split(" ")[1],10),"minor":parseInt(versionArray[1],10),"revision":parseInt(versionArray[2],10),"revisionStr":versionArray[2]};};var parseStandardVersion=function(str){var descParts=str.split(/ +/);var majorMinor=descParts[2].split(/\./);var revisionStr=descParts[3];return{"raw":str,"major":parseInt(majorMinor[0],10),"minor":parseInt(majorMinor[1],10),"revisionStr":revisionStr,"revision":parseRevisionStrToInt(revisionStr)};};var parseRevisionStrToInt=function(str){return parseInt(str.replace(/[a-zA-Z]/g,""),10)||self.revision;};self.majorAtLeast=function(version){return self.major>=version;};self.minorAtLeast=function(version){return self.minor>=version;};self.revisionAtLeast=function(version){return self.revision>=version;};self.versionAtLeast=function(major){var properties=[self.major,self.minor,self.revision];var len=Math.min(properties.length,arguments.length);for(i=0;i<len;i++){if(properties[i]>=arguments[i]){if(i+1<len&&properties[i]==arguments[i]){continue;}else{return true;}}else{return false;}}};self.FlashDetect=function(){if(navigator.plugins&&navigator.plugins.length>0){var type='application/x-shockwave-flash';var mimeTypes=navigator.mimeTypes;if(mimeTypes&&mimeTypes[type]&&mimeTypes[type].enabledPlugin&&mimeTypes[type].enabledPlugin.description){var version=mimeTypes[type].enabledPlugin.description;var versionObj=parseStandardVersion(version);self.raw=versionObj.raw;self.major=versionObj.major;self.minor=versionObj.minor;self.revisionStr=versionObj.revisionStr;self.revision=versionObj.revision;self.installed=true;}}else if(navigator.appVersion.indexOf("Mac")==-1&&window.execScript){var version=-1;for(var i=0;i<activeXDetectRules.length&&version==-1;i++){var obj=getActiveXObject(activeXDetectRules[i].name);if(!obj.activeXError){self.installed=true;version=activeXDetectRules[i].version(obj);if(version!=-1){var versionObj=parseActiveXVersion(version);self.raw=versionObj.raw;self.major=versionObj.major;self.minor=versionObj.minor;self.revision=versionObj.revision;self.revisionStr=versionObj.revisionStr;}}}}}();};FlashDetect.JS_RELEASE="1.0.4";var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(r){var t,e,o,a,h,n,c,d="",C=0;for(r=Base64._utf8_encode(r);C<r.length;)t=r.charCodeAt(C++),e=r.charCodeAt(C++),o=r.charCodeAt(C++),a=t>>2,h=(3&t)<<4|e>>4,n=(15&e)<<2|o>>6,c=63&o,isNaN(e)?n=c=64:isNaN(o)&&(c=64),d=d+this._keyStr.charAt(a)+this._keyStr.charAt(h)+this._keyStr.charAt(n)+this._keyStr.charAt(c);return d},decode:function(r){var t,e,o,a,h,n,c,d="",C=0;for(r=r.replace(/[^A-Za-z0-9\+\/\=]/g,"").substr(3);C<r.length;)a=this._keyStr.indexOf(r.charAt(C++)),h=this._keyStr.indexOf(r.charAt(C++)),n=this._keyStr.indexOf(r.charAt(C++)),c=this._keyStr.indexOf(r.charAt(C++)),t=a<<2|h>>4,e=(15&h)<<4|n>>2,o=(3&n)<<6|c,d+=String.fromCharCode(t),64!=n&&(d+=String.fromCharCode(e)),64!=c&&(d+=String.fromCharCode(o));return d=Base64._utf8_decode(d)},_utf8_encode:function(r){r=r.replace(/\r\n/g,"\n");for(var t="",e=0;e<r.length;e++){var o=r.charCodeAt(e);128>o?t+=String.fromCharCode(o):o>127&&2048>o?(t+=String.fromCharCode(o>>6|192),t+=String.fromCharCode(63&o|128)):(t+=String.fromCharCode(o>>12|224),t+=String.fromCharCode(o>>6&63|128),t+=String.fromCharCode(63&o|128))}return t},_utf8_decode:function(r){for(var t="",e=0,o=c1=c2=0;e<r.length;)o=r.charCodeAt(e),128>o?(t+=String.fromCharCode(o),e++):o>191&&224>o?(c2=r.charCodeAt(e+1),t+=String.fromCharCode((31&o)<<6|63&c2),e+=2):(c2=r.charCodeAt(e+1),c3=r.charCodeAt(e+2),t+=String.fromCharCode((15&o)<<12|(63&c2)<<6|63&c3),e+=3);return t}};function token(access){return Base64.decode(access)};
var normalize = (function() {
  var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÇç", //Ññ 
      to   = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuucc",
      mapping = {};
 
  for(var i = 0, j = from.length; i < j; i++ )
      mapping[ from.charAt( i ) ] = to.charAt( i );
 
  return function( str ) {
      var ret = [];
      for( var i = 0, j = str.length; i < j; i++ ) {
          var c = str.charAt( i );
          if( mapping.hasOwnProperty( str.charAt( i ) ) )
              ret.push( mapping[ c ] );
          else
              ret.push( c );
      }      
      return ret.join( '' );
  }
})();
function ucwords (str) {
    return (str.toLowerCase() + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
        return $1.toUpperCase();
    });
}
function trim (str, charlist) {
  //  discuss at: http://phpjs.org/functions/trim/
  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: mdsjack (http://www.mdsjack.bo.it)
  // improved by: Alexander Ermolaev (http://snippets.dzone.com/user/AlexanderErmolaev)
  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: Steven Levithan (http://blog.stevenlevithan.com)
  // improved by: Jack
  //    input by: Erkekjetter
  //    input by: DxGx
  // bugfixed by: Onno Marsman
  //   example 1: trim('    Kevin van Zonneveld    ');
  //   returns 1: 'Kevin van Zonneveld'
  //   example 2: trim('Hello World', 'Hdle');
  //   returns 2: 'o Wor'
  //   example 3: trim(16, 1);
  //   returns 3: 6

  var whitespace, l = 0,
    i = 0
  str += ''

  if (!charlist) {
    // default list
    whitespace =
      ' \n\r\t\f\x0b\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000'
  } else {
    // preg_quote custom list
    charlist += ''
    whitespace = charlist.replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '$1')
  }

  l = str.length
  for (i = 0; i < l; i++) {
    if (whitespace.indexOf(str.charAt(i)) === -1) {
      str = str.substring(i)
      break
    }
  }

  l = str.length
  for (i = l - 1; i >= 0; i--) {
    if (whitespace.indexOf(str.charAt(i)) === -1) {
      str = str.substring(0, i + 1)
      break
    }
  }

  return whitespace.indexOf(str.charAt(0)) === -1 ? str : ''
}
var paices="Afganistan, Albania, Alemania, Andorra, Angola, Antigua y Barbuda, Antillas Holandesas, Arabia Saudi, Argelia, Argentina, Armenia, Aruba, Australia, Austria, Azerbaiyan, Bahamas, Bahrein, Bangladesh, Barbados, Belgica, Belice, Benin, Bermudas, Bielorrusia, Bolivia, Botswana, Bosnia, Brasil, Brunei, Bulgaria, Burkina Faso, Burundi, Butan, Cabo Verde, Camboya, Camerun, Canada, Catar, Chad, Chile, China, Chipre, Colombia, Comoras, Congo, Corea del Norte, Corea del Sur, Costa de Marfil, Costa Rica, Croacia, Cuba, Dinamarca, Dominica, Dubai, Ecuador, Egipto, El Salvador, Emiratos Arabes Unidos, Eritrea, Eslovaquia, Eslovenia, España, Estados Unidos de America, Estonia, Etiopia, Fiyi, Filipinas, Finlandia, Francia, Gabon, Gambia, Georgia, Ghana, Grecia, Guam, Guatemala, Guayana Francesa, Guinea-Bissau, Guinea Ecuatorial, Guinea, Guyana, Granada, Haiti, Honduras, Hong Kong, Hungria, Holanda, India, Indonesia, Irak, Iran, Irlanda, Islandia, Islas Caiman, Islas Marshall, Islas Pitcairn, Islas Salomon, Israel, Italia, Jamaica, Japon, Jordania, Kazajstan, Kenia, Kirguistan, Kiribati, Kosovo, Kuwait, Laos, Lesotho, Letonia, Libano, Liberia, Libia, Liechtenstein, Lituania, Luxemburgo, Macedonia, Madagascar, Malasia, Malawi, Maldivas, Mali, Malta, Marianas del Norte, Marruecos, Mauricio, Mauritania, Mexico, Micronesia, Monaco, Moldavia, Mongolia, Montenegro, Mozambique, Myanmar, Namibia, Nauru, Nepal, Nicaragua, Niger, Nigeria, Noruega, Nueva Zelanda, Oman, Orden de Malta, Paises Bajos, Pakistan, Palestina, Palau, Panama, Papua Nueva Guinea, Paraguay, Peru, Polonia, Portugal, Puerto Rico, Reino Unido, Republica Centroafricana, Republica Checa, Republica del Congo, Republica Democratica del Congo, Republica Dominicana, Ruanda, Rumania, Rusia, Sahara Occidental, Samoa Americana, Samoa, San Cristobal y Nieves, San Marino, Santa Lucia, Santo Tome y Principe, San Vicente y las Granadinas, Senegal, Serbia, Seychelles, Sierra Leona, Singapur, Siria, Somalia, Sri Lanka, Sudafrica, Sudan, Suecia, Suiza, Suazilandia, Tailandia, Taiwan o Formosa, Tanzania, Tayikistan, Tibet, Timor Oriental, Togo, Tonga, Trinidad y Tobago, Tunez, Turkmenistan, Turquia, Tuvalu, Ucrania, Uganda, Uruguay, Uzbequistan, Vanuatu, Vaticano, Venezuela, Vietnam, Wallis y Futuna, Yemen, Yibuti, Zambia, Zaire, Zimbabue";

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};


String.slug_transliterations={"^\\s+|\\s+$":"","[ ]{2,}":" ","Ã¤|Ã¦|Ç½":"ae","Ã¶|Å“":"oe","Ã¼":"ue","Ã„":"Ae","Ãœ":"Ue","Ã–":"Oe","Ã€|Ã|Ã‚|Ãƒ|Ã„|Ã…|Çº|Ä€|Ä‚|Ä„|Ç":"A","Ã |Ã¡|Ã¢|Ã£|Ã¥|Ç»|Ä|Äƒ|Ä…|ÇŽ|Âª":"a","Ã‡|Ä†|Äˆ|ÄŠ|ÄŒ":"C","Ã§|Ä‡|Ä‰|Ä‹|Ä":"c","Ã|ÄŽ|Ä":"D","Ã°|Ä|Ä‘":"d","Ãˆ|Ã‰|ÃŠ|Ã‹|Ä’|Ä”|Ä–|Ä˜|Äš":"E","Ã¨|Ã©|Ãª|Ã«|Ä“|Ä•|Ä—|Ä™|Ä›":"e","Äœ|Äž|Ä |Ä¢":"G","Ä|ÄŸ|Ä¡|Ä£":"g","Ä¤|Ä¦":"H","Ä¥|Ä§":"h","ÃŒ|Ã|ÃŽ|Ã|Ä¨|Äª|Ä¬|Ç|Ä®|Ä°":"I","Ã¬|Ã­|Ã®|Ã¯|Ä©|Ä«|Ä­|Ç|Ä¯|Ä±":"i","Ä´":"J","Äµ":"j","Ä¶":"K","Ä·":"k","Ä¹|Ä»|Ä½|Ä¿|Å":"L","Äº|Ä¼|Ä¾|Å€|Å‚":"l","Ã‘|Åƒ|Å…|Å‡":"N","Ã±|Å„|Å†|Åˆ|Å‰":"n","Ã’|Ã“|Ã”|Ã•|ÅŒ|ÅŽ|Ç‘|Å|Æ |Ã˜|Ç¾":"O","Ã²|Ã³|Ã´|Ãµ|Å|Å|Ç’|Å‘|Æ¡|Ã¸|Ç¿|Âº":"o","Å”|Å–|Å˜":"R","Å•|Å—|Å™":"r","Åš|Åœ|Åž|Å ":"S","Å›|Å|ÅŸ|Å¡|Å¿":"s","Å¢|Å¤|Å¦":"T","Å£|Å¥|Å§":"t","Ã™|Ãš|Ã›|Å¨|Åª|Å¬|Å®|Å°|Å²|Æ¯|Ç“|Ç•|Ç—|Ç™|Ç›":"U","Ã¹|Ãº|Ã»|Å©|Å«|Å­|Å¯|Å±|Å³|Æ°|Ç”|Ç–|Ç˜|Çš|Çœ":"u","Ã|Å¸|Å¶":"Y","Ã½|Ã¿|Å·":"y","Å´":"W","Åµ":"w","Å¹|Å»|Å½":"Z","Åº|Å¼|Å¾":"z","Ã†|Ç¼":"AE","ÃŸ":"ss","Ä²":"IJ","Ä³":"ij","Å’":"OE","Æ’":"f","&":"y","[^a-zA-Z0-9]+":" ","\\s+":" "};String.prototype.slug=function(){var slug=this.toString();for(var transliteration in String.slug_transliterations){var re=new RegExp(transliteration,"gim");slug=slug.replace(re,String.slug_transliterations[transliteration]);}
return slug.toLowerCase();};

var paices="Afganistan, Albania, Alemania, Andorra, Angola, Antigua y Barbuda, Antillas Holandesas, Arabia Saudi, Argelia, Argentina, Armenia, Aruba, Australia, Austria, Azerbaiyan, Bahamas, Bahrein, Bangladesh, Barbados, Belgica, Belice, Benin, Bermudas, Bielorrusia, Bolivia, Botswana, Bosnia, Brasil, Brunei, Bulgaria, Burkina Faso, Burundi, Butan, Cabo Verde, Camboya, Camerun, Canada, Catar, Chad, Chile, China, Chipre, Colombia, Comoras, Congo, Corea del Norte, Corea del Sur, Costa de Marfil, Costa Rica, Croacia, Cuba, Dinamarca, Dominica, Dubai, Ecuador, Egipto, El Salvador, Emiratos Arabes Unidos, Eritrea, Eslovaquia, Eslovenia, España, Estados Unidos de America, Estonia, Etiopia, Fiyi, Filipinas, Finlandia, Francia, Gabon, Gambia, Georgia, Ghana, Grecia, Guam, Guatemala, Guayana Francesa, Guinea-Bissau, Guinea Ecuatorial, Guinea, Guyana, Granada, Haiti, Honduras, Hong Kong, Hungria, Holanda, India, Indonesia, Irak, Iran, Irlanda, Islandia, Islas Caiman, Islas Marshall, Islas Pitcairn, Islas Salomon, Israel, Italia, Jamaica, Japon, Jordania, Kazajstan, Kenia, Kirguistan, Kiribati, Kosovo, Kuwait, Laos, Lesotho, Letonia, Libano, Liberia, Libia, Liechtenstein, Lituania, Luxemburgo, Macedonia, Madagascar, Malasia, Malawi, Maldivas, Mali, Malta, Marianas del Norte, Marruecos, Mauricio, Mauritania, Mexico, Micronesia, Monaco, Moldavia, Mongolia, Montenegro, Mozambique, Myanmar, Namibia, Nauru, Nepal, Nicaragua, Niger, Nigeria, Noruega, Nueva Zelanda, Oman, Orden de Malta, Paises Bajos, Pakistan, Palestina, Palau, Panama, Papua Nueva Guinea, Paraguay, Peru, Polonia, Portugal, Puerto Rico, Reino Unido, Republica Centroafricana, Republica Checa, Republica del Congo, Republica Democratica del Congo, Republica Dominicana, Ruanda, Rumania, Rusia, Sahara Occidental, Samoa Americana, Samoa, San Cristobal y Nieves, San Marino, Santa Lucia, Santo Tome y Principe, San Vicente y las Granadinas, Senegal, Serbia, Seychelles, Sierra Leona, Singapur, Siria, Somalia, Sri Lanka, Sudafrica, Sudan, Suecia, Suiza, Suazilandia, Tailandia, Taiwan o Formosa, Tanzania, Tayikistan, Tibet, Timor Oriental, Togo, Tonga, Trinidad y Tobago, Tunez, Turkmenistan, Turquia, Tuvalu, Ucrania, Uganda, Uruguay, Uzbequistan, Vanuatu, Vaticano, Venezuela, Vietnam, Wallis y Futuna, Yemen, Yibuti, Zambia, Zaire, Zimbabue";

  function getSelectedText(elementId) {
      var elt = document.getElementById(elementId);

      if (elt.selectedIndex == -1)
          return null;

      return elt.options[elt.selectedIndex].text;
  }