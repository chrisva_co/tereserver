<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('ubicacions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('categorias')->nullable()->default("general"); 

            $table->integer('posX');
            $table->integer('posY');
            $table->integer('ancho');
            $table->integer('alto');
        });

 
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password', 60)->nullable();
            $table->string('socials_id', 1000)->unique()->nullable();
            $table->string('avatar');
            $table->integer('role')->nullable()->default(0); //0=usuario 1=Admin 2=Baneado 3=bot

            $table->string('telefono')->nullable();
            $table->string('direccion')->nullable();
            $table->string('ubicacion')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });



        Schema::create('productos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->index(); //administrador
            $table->integer('barCode')->nullable(); //

            $table->string('nombre'); //
            $table->string('slug')->unique();
            $table->string('descripcion', 1000)->nullable(); //alineaciones y cosas asi
            $table->integer('valorUnitario');
            $table->string('categorias')->nullable()->default("general"); 
            $table->string('formato')->nullable();
            $table->string('image')->nullable();
            $table->string('ubicacion')->nullable();
            $table->string('marca')->nullable();
            $table->boolean('activo')->nullable()->default(true);
            $table->boolean('conIva')->nullable()->nullable()->default(true);//el iva lo saca de config "19%"
            $table->string('tipo', 20)->nullable();
            $table->string('extra')->nullable(); //canal o post

            $table->timestamps();
        });


        
        Schema::create('facturas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->index(); //administrador

            $table->string('productos',9000); //[{"id":0,"nombre":"Leche Lactosada","iva":19,"cantidad":4},{"id":1,"nombre":"Leche Lactosada","iva":19,"cantidad:1"}]

            $table->string('informacionDePago', 5000)->nullable(); 
            $table->string('informacionDeEnvio', 5000)->nullable(); 

            $table->boolean('pagado')->nullable()->default(false);
            $table->boolean('enviado')->nullable()->default(false);

            $table->string('extra')->nullable(); //canal o post
            $table->timestamps();
        });


        
        Schema::create('config', function (Blueprint $table) {  //el iva
            $table->increments('id');
            $table->string('nombre', 255);
            $table->string('descripcion', 5000);
            $table->boolean('activo')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
