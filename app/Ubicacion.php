<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DateTime;
#use Illuminate\Support\Str;

class Ubicacion extends Model
{	

    protected $fillable = ['nombre', 'categorias', 'sizeX', 'sizeY', 'mapa'];

	public $timestamps = false;

	public function saveSecret() {
        $this->timestamps = false;
        $this->save();
    }

}
