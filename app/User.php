<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = ['name', 'email', 'password','socials_id', 'avatar'];

    /**
     * The attributes that should be hidden for arrays.
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function cantidadFacturas(){
        return 300;
    }


    public function facturas() //canciones de cantante
    {
        return $this->hasMany('App\Factura');
    }


//0=usuario 1=Admin 2=Baneado 3=bot
    public function role(){
        
        switch ($this->attributes['role']) {
            case 0:
                return 'Cliente';
            case 1:
                return 'Admin';
            case 2:
                return 'Baneado';
            case 3:
                return 'Bot';
        }
    }
}
