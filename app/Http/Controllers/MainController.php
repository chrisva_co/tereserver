<?php
/*
Relaciones y LISTAS! https://stackoverflow.com/questions/16210628/laravel-relationships
*/
namespace App\Http\Controllers;

use App\Producto;
use App\Ubicacion;
use App\Factura;
use App\User;
use App\Config;
use Auth;
use Validator;
use Illuminate\Http\Request;
use Lang;
use Illuminate\Http\Response;
use DB;
use Helpers;
use Epayco\Epayco;




class MainController extends Controller{



	private function esAdmin(){
		if (Auth::check())
			if (Auth::user()->role==1)
				return true;
		return false;
	}

	public function getPrincipal(){
		if ($this->esAdmin()){
			return redirect('productos');
		}
		elseif (Auth::check()){
		    
			if ($this->estaPagandoUnaFactura() && !$this->estaPagandoUnaFactura(true)){
				return $this->getLoginDesdeApp();
				//se generaba un error cuando no existe la factura
				//$factura = Factura::where("user_id", Auth::user()->id)->where("pagado", false)->orderBy('updated_at', 'desc')->first();
				//return redirect("factura/".$factura->id."?m=1");
			}
			
			return redirect('facturas/'.Auth::user()->id); //redirecciona a la zona de usuarios
		}
		elseif($this->estaPagandoUnaFactura()) //si es un visitante que esta pagando factura
		    return redirect("app/login");
		    
		return view("entrar");
	}

	//Solo para pagar
	public function getLoginDesdeApp(){

		//si ya inicio sesion, 
		if (Auth::check()){
            
            //Actualizo telefono
            $user = Auth::user();
            if ($user->telefono=="" || $user->telefono==null){
                $user->telefono = $this->getUserAgent(2);
                $user->save();
            }
            
            if ($this->estaPagandoUnaFactura(true)){
             return redirect('facturas/'.Auth::user()->id); //redirecciona a la zona de usuarios   
            }
            
            //AQUI GESTIONO LO DE LAS FACTURAS RECHAZADA, PENDIENTE, PAGADA Y FINALIZADA
            
			//busco ultima factura sin pagar del usuario
			$factura = Factura::where("user_id", Auth::user()->id)
					->where("pagado", false)->where("informacionDePago", null)->orderBy('updated_at', 'desc')->first();
					
		/*	if ($factura->informacionDePago){
		        $factura->informacionDePago=json_decode($factura->informacionDePago);
		        $factura = null;
			}*/


			if (!$factura){ 
				//si no existe, creo una con los id del userAgent

				$factura = new Factura();
				$factura->user_id=Auth::user()->id;
				$factura->productos = $this->generarJsonProductosDeFactura($this->getUserAgent(1));
				$factura->save();

			}else{
				//si existe, actualizo la factura sin pagar con los id del userAgent
				$factura->productos = $this->generarJsonProductosDeFactura($this->getUserAgent(1));
				$factura->save();
			}

			return redirect("factura/".$factura->id."?m=1");
		}

		return view("entrar-app");
	}

	private function estaPagandoUnaFactura($laAppQuiereVerFactura=false){
		$uan = $this->getUserAgent();
		
		if ($laAppQuiereVerFactura)
            return stripos($uan, ";;;facturas")!==false;
            
		$ua = explode(";;;", $uan);
		return count($ua)>2;
	}

	private function generarJsonProductosDeFactura($ids){
		$ids = trim(trim($ids),",");
		$ids = explode(",", $ids);

		$productos = Producto::where('id',explode(":", $ids[0])[0]);
		foreach ($ids as $id) {
			$productos->orWhere("id", explode(":", $id)[0]);
		}

		$productos = $productos->get();
		$config = $this->objetoConfiguracion();

		$lista = "[";
		foreach ($productos as $i => $p) {
			$cant = explode(":", $ids[$i])[1];

			$lista.='{"id":'.$p->id.
					',"nombre":"'.$p->nombre.
					'","iva":'.$config->iva.
					',"cantidad":'.$cant.
					',"precio":'.($p->valorUnitario*$cant).'},';
		}

		return trim($lista,",")."]";
		/*[{"id":0,"nombre":"Leche Lactosada","iva":19,"cantidad":4,"precio":2500},{"id":1,"nombre":"Leche Lactosada","iva":0,"cantidad":1,"precio":3404}]*/
	}

	private function getUserAgent($ids=false){
		$userAgent=$_SERVER['HTTP_USER_AGENT']; //.';;;2:1,3:2,4:5,'

		if ($ids!==false)
			return explode(";;;", $userAgent)[$ids];
		return $userAgent;
	}





	//mostrar un evento
	public function getConfiguracion(){
		if (!$this->esAdmin()){
			return redirect("/");
		}
		
		$config = $this->objetoConfiguracion(1);
		$post = Config::find(1);
		return view("config", compact('post', 'config'));
	}

	public function postConfiguracion(Request $request, $id=''){
		
		$data=$request->all();
		$rules = array(
				'descripcion' => 'required'
			);
		$this->validate($request, $rules);
	    $data['descripcion']=stripslashes($data['descripcion']);
	    $data['descripcion']=str_replace('"[','[', $data['descripcion']);
	    $data['descripcion']=str_replace(']"',']', $data['descripcion']);
	   
	    //dd($data['descripcion']);
	    
		//Creando
			$post = Config::find($data['id']);
			$post->update($data);
		
		return "Actualizado";
		
		$msg='actualizados <a onclick="parent.location.reload()"> <b>Recargar</b> </a>';

		return redirect("configuracion")->with('success', 'Actualizado! ');
	}




	//mostrar un producto
	public function getProducto($id=''){
		$post = Producto::find($id);
		$config = $this->objetoConfiguracion();
		return view("posts/form-producto", compact('post','config'));
	}

	public function postProducto(Request $request, $id=''){
		
		$data=$request->all();

		$rules = array(
				'nombre' => 'required|min:3|max:180',
				'image' => 'required',
				'barCode' => ''
			);
		$this->validate($request, $rules);
	
		//Creando
		if (!$id){
			$post = new Producto($data);
			$post->user_id=1;
			$post->save();

			$msg='guardados <a target="_blank" href="producto/'.$post->slug.'"></a>';
		}else{
		//Editando
			$post = Producto::find($id);
			$post->update($data);
			$msg='actualizados <a onclick="parent.location.reload()"> <b>Recargar</b> </a>';
		}	
		return redirect("producto/".$post->id)->with('success', 'Datos '.$msg);
	}

	//post/id/eliminar
	public function getProducto_eliminar($id){

		if ($this->esAdmin()){
			Producto::destroy($id);
			return redirect("/");
		}
		return 0;
	}



//ADMIN
	public function getProductos(){
		if (!$this->esAdmin()){
			return redirect("/");
		}
		$productos = Producto::orderBy('id','DESC')->paginate(30);
		$totalProductos = Producto::count();
		
		$config = $this->objetoConfiguracion();

		return view("productos", compact('productos', 'config','totalProductos'));
	}

//ADMIN
	public function getUsuarios(){
		if (!$this->esAdmin()){
			return redirect("/");
		}
		$usuarios = User::orderBy('id','DESC')->paginate(30);
		$total = User::count();
		$config = $this->objetoConfiguracion();

		return view("usuarios", compact('usuarios', 'config','total'));
	}

	public function getFacturas($user_id=0){

		/*$epayco = new Epayco(array(
		    "apiKey" => "YOU_PUBLIC_API_KEY",
		    "privateKey" => "YOU_PRIVATE_API_KEY",
		    "lenguage" => "ES",
		    "test" => true
		));

		dd($epayco);*/

		if (!Auth::check())	return redirect("/");

		if (!$this->esAdmin())
			if (Auth::user()->id!=$user_id)
				return redirect("/");
		
		$facturas = Factura::orderBy('id','DESC');

		if ($user_id)
			$facturas->where('user_id', $user_id);

		$facturas = $facturas->paginate(20);

		$total = Factura::count();
		$config = $this->objetoConfiguracion();

		if ($this->estaPagandoUnaFactura())
			$view="facturas-app";
		else
			$view="facturas";
		return view($view, compact('facturas', 'config','total'));
	}




	
	public function getFactura($id){
		if (!Auth::check())	return redirect("/");
		$m = isset($_GET['m'])?1:0;
		
		$factura = Factura::findOrFail($id);
		
		//si no es el dueño de la factura o un administrador 
		    //no la puede ver
	    if (!Auth::check())
	        return redirect("/");
	    
	    elseif(Auth::user()->id!=$factura->user_id) //si no es el dueño
	        if (!$this->esAdmin()) //tampoco administrador
	            return redirect("/");
	    
	   
		    
		$config = $this->objetoConfiguracion();

		$factura->productos = json_decode($factura->productos);
        
        if ($factura->informacionDePago)
		    $factura->informacionDePago=json_decode($factura->informacionDePago);
		
		
		return view("factura", compact('factura', 'config', 'm'));
	}

	//este post queda inhabilitado por el uso de pagarFactura via Epayco
	public function postFactura(Request $request, $id){
		//if (!Auth::check())	return redirect("/");

		$factura = Factura::findOrFail($id);

		if (!$this->esAdmin())
			if (Auth::user()->id != $factura->user_id)
				return redirect("/");


		$config = $this->objetoConfiguracion();
		$factura->pagado=true;
		$factura->save();

		return $this->getFactura($id);
	}

	private function pagarFactura($infoTransaccion, $id){

		$factura = Factura::findOrFail($id);
		/*
		if (!$this->esAdmin())
			if (Auth::user()->id != $factura->user_id)
				return redirect("/");*/

		$config = $this->objetoConfiguracion();
		
		$factura->informacionDePago=json_encode($infoTransaccion);
		$factura->pagado=($infoTransaccion->x_cod_response == 1);
		$factura->save();

		return redirect(asset('factura/'.$id.'?m=1'));
	}
	
	//tere.paginaweb2.com/factura/30/epayco?ref_payco=7c322447aa822fa2a336bbfe
	public function getPagoDeEpayco(Request $request, $id){
        //dd($request->all());
		if (isset($_GET['ref_payco'])){
			$respuesta = file_get_contents('https://secure.epayco.co/validation/v1/reference/'.$_GET['ref_payco']);
			$respuesta = json_decode($respuesta);

			if ($respuesta->success){
				  //Transaccion aprobada
				  if ($respuesta->data->x_cod_response == 1) {
				  }
				  //Transaccion rechazada
				  elseif ($respuesta->data->x_cod_response == 2) {
				  }
				  //Transaccion Pendiente
				  elseif ($respuesta->data->x_cod_response == 3) {
				  }
				  //Transaccion Fallida
				  elseif ($respuesta->data->x_cod_response == 4) {
				  }

				  return $this->pagarFactura($respuesta->data, $id);
			}
		}
		return $this->getFactura($id);
	}	



	public function objetoConfiguracion($array=0){
		$config=Config::find(1);
		$descripcion = json_decode($config->descripcion, $array);

		$nuevaCat = array();
		
    	$descCat = ($array)?$descripcion['categorias']:$descripcion->categorias;
		
		foreach ($descCat as $key => $categoria) {
		    $categoria = trim($categoria);
			$nuevaCat[str_slug($categoria)]=$categoria;
		}
		if ($array)
		    $descripcion['categorias'] = $nuevaCat;
		else
	    	$descripcion->categorias = $nuevaCat;
	    	
		return $descripcion;
	}




	public function getPagar($idFactura){
		//
		//Se muestra la factura que se desea pagar y sus metodos de pago
	}

	public function pagoProcesado(){
		//se obtiene el estado de la factura, si dice que pago, entonces se cambia la factura como pagada
	}

	public function postPrepararPagar(Request $request){
		//Elimino facturas que no esten pagas

		//recibe y guarda la informacion del la factura "productos"

		//Redirecciona a getPagar para que el usuario page
	}





	//JSON DE LAS APPS
	public function escanearProducto(Request $request){
	    $data=$request->all();
		$post = Producto::where("barCode", $data['barCode'])->first();
		if ($post)
		    return $post->toJson();
	}
	
	public function getProductoPagado($id){
	    try{
    	    $post = Factura::find($id);
    	    return $post->pagado;
	    }catch(\Exception $e){
	        return 0;
	    }
	}


	public function getJsonProductos($fila='', $valor=''){
		//categoria //deportes / cultural
		if ($fila!='')
			$posts=Producto::where($fila, "like", '%'.$valor.'%')->orderBy('id', 'DESC')->get()->take(30);
		else
			$posts=Producto::get()->take(30);
		return $posts->toJson();
	}
	//4:1,3:4,5:2, id:cant,
	public function getRecomendacion($ids, $cantidadRecomendaciones=3){
	    
        
	    //Select 
	    $ids=explode(',',trim($ids,','));
	    
	    if (!count($ids))
	        return '';
	    
	    $facturas = Factura::select('productos')->where('pagado', 1);
		foreach ($ids as $id) {
		    
		//	$facturas->where('productos', 'LIKE', '%\"id\":'.$id.',%');
        	$facturas->where('productos', 'LIKE', '%\"id\":'.explode(":", $id)[0].',%');
		} 
		$facturas = $facturas->inRandomOrder()->get()->take(30);
		
		if(!$facturas->count()) 
		    return '';
	    
	    $productosUF=array();
	    foreach ($facturas as $factura) {
		    $productosUF[]= $factura->productos;
		} 
		

        $nuevosIds = recomendarP($productosUF, $ids);
        
        
        $productos = Producto::select('id','nombre','valorUnitario','image','categorias','ubicacion')->where('id', $nuevosIds[0]);
		foreach ($nuevosIds as $nuevoId) {
			$productos->orWhere('id', $nuevoId);
		} 
		$productos = $productos->get()->take($cantidadRecomendaciones);
		
		//dd($productos->toJson());
	    
			
		return $productos->toJson();
	}
	

	public function postJsonProductos(Request $request){
		//categoria //deportes / cultural

		$data=$request->all();

		if (isset($data['fila']))
			$posts=Producto::where($data['fila'], "like", '%'.$data['valor'].'%')->orderBy('id', 'DESC')->get()->take(30);
		else
			$posts=Producto::get()->take(30);

		return $posts->toJson();
	}



	public function respuestaJSON($tipo, $mensaje){
		$cabecera='"tipo":"'.$tipo.'","msg":"'.$mensaje.'"';
			$userJson="";
			if (Auth::check())
				$userJson=', "dato":['.Auth::user()->toJson().']';

			$json='{'.$cabecera.$userJson.'}';
			return $json;
	}

	public function getUbicaciones(){
	    if (!Auth::check())
	        return '';
	        
	    $ubicacion=Ubicacion::find(1);
	    $config = $this->objetoConfiguracion();
	    
	    $size[] = array("380"=>"380","480"=>"480","580"=>"580","680"=>"680","780"=>"780");
		$size[] = array("380"=>"380","480"=>"480","580"=>"580","680"=>"680","780"=>"780");
		
		return view("ubicacion", compact('ubicacion', 'size', 'config'));
	}
	
	public function getUbicacion($categoria=""){
	   
	    $_GET['iframe-base']=1;
	    $_GET['sinvp']=1;
	    $ubicacion=Ubicacion::find(1);
	    $config = $this->objetoConfiguracion();
		return view("ubicacion2", compact('ubicacion', 'config', 'categoria'));
	}
	
	public function postUbicaciones(Request $request){
	    
	    	if (Auth::user()->role!=1)
	    	    return "No tienes permiso de estar aqui";
	    	
		$data=$request->all();
		
		 $ubicacion=Ubicacion::find(1);
		 $ubicacion->sizeX=$data['sizeX'];
		 $ubicacion->sizeY=$data['sizeY'];
		 $ubicacion->mapa=$data['mapa'];
		 
		 if ($ubicacion->save())
		    return "Datos guardados";
		 
		 return "Error al guardar datos";
	}
}
