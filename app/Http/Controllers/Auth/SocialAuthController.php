<?php


namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Socialite;
use Auth;
use Google_Client;
use Google_Service_People;

class SocialAuthController extends Controller
{


	public function principal(){
		return "este controlador es todo lo relacionado con login y registro SOCIAL!";
	}


	/*FACEBOOOOK*//////////////////////////////
     /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return Response
     */
    public function redirectToProvider($provedor)
    {
        return Socialite::driver($provedor)->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return Response
     */
    public function handleProviderCallback($proveedor)
    {
        try{
            $user = Socialite::driver($proveedor)->user();
            
            if ($user==null)
                return redirect('/');
        
        }catch(\Exception $ex){
            return redirect('/')->with('error', 'Error: '.$ex->getMessage());
        }
        try{
            return $this->findOrCreateUser($proveedor, $user);
         }catch(\Exception $ex){
            return redirect('/')->with('error', 'Error: SQL: '.$ex->getMessage());
        }
    }

    /**
     * Return user if exists; create and return if doesn't
     *
     * @param $facebookUser
     * @return User
     * puede generar un error en socials_id si los caracteres superan mas de 3000
     */
    private function findOrCreateUser($proveedor, $callUser)
    {
        if ($proveedor=='' or $callUser=='')
            return  redirect('/');

        $authUser='';
        if ($callUser->email)
            $authUser = User::where('email', $callUser->email)->first();


        if (!$authUser){//buscar por id
            $authUser = User::where('socials_id', 'LIKE', '%'.$proveedor.'::'.$callUser->id.'%')->first();
        }
        
        if (!$authUser){
            $authUser = User::create([
                'name' => $callUser->name,
                'email' => ($callUser->email)?$callUser->email:time().'@ramdon_'.$proveedor.'.com',
                'avatar' => $callUser->avatar,
                'socials_id' => $proveedor.'::'.$callUser->id."\n"
            ]);

            Auth::login($authUser);
            return redirect('/');
        }

        //si no esta el proveedor con su id, lo agrega y guarda.
        if(stripos($authUser->socials_id, $proveedor.'::'.$callUser->id)===false){
            $authUser->socials_id.=$proveedor.'::'.$callUser->id."\n";
            $authUser->save();
        }

        Auth::login($authUser);
        return  redirect('/');
    }
}