<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DateTime;
#use Illuminate\Support\Str;

class Config extends Model
{	

	protected $table = 'config';
    protected $fillable = ['nombre', 'descripcion', 'activo'];

	public $timestamps = false;

	public function saveSecret() {
        $this->timestamps = false;
        $this->save();
    }

}
