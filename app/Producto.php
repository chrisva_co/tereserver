<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use App\Cantante;
use DateTime;
use Illuminate\Support\Str;

class Producto extends Model
{	
	use Sluggable;
    protected $fillable = ['user_id', 'barCode', 'nombre', 'descripcion', 'valorUnitario','categorias', 'formato','image','ubicacion','marca','activo','conIva','tipo','extra'];




	public function sluggable(){
        return [
            'slug' => ['source' => 'nombre']
        ];
    }


    public function getImage($sizeUr=''){
		if (stripos($this->attributes['image'], 'imgur.'))
			return $this->attributes['image'].$sizeUr.'.jpg';
		return $this->attributes['image'];
	}

	public function conIva(){
		return ($this->attributes['conIva']==1)?"Si":"No";
	}

	public function categorias(){
		return str_replace('-', ' ', ucfirst($this->attributes['categorias']));
	}
}
