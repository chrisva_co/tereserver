<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DateTime;
use Illuminate\Support\Str;

class Factura extends Model
{	
    protected $fillable = ['user_id', 'productos', 'informacionDePago', 'informacionDeEnvio', 'pagado','enviado', 'extra'];

	public function getCantidadFacturas(){
		return 3;
	}

	public function conIva(){
		return ($this->attributes['conIva']==1)?"Si":"No";
	}

	public function categorias(){
		return str_replace('-', ' ', ucfirst($this->attributes['categorias']));
	}


    public function user(){
        return $this->belongsTo('App\User');
    }

    public function getCantidadPrecio(){

    	$array = array('valorTotal'=>0,'cantidadTotal'=>0);
    	$productos = json_decode($this->attributes['productos']);

    	foreach ($productos as $producto) {
			$array['valorTotal']+=$producto->precio;     //el precio es valorUnitario*cantidad
			$array['cantidadTotal']+=$producto->cantidad;
    	}


    	return $array;
    }
}
